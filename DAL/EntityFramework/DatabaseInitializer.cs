﻿using DAL.Entities;
using System.Data.Entity;

namespace DAL.EntityFramework
{
    /// <summary>
    /// Initialize database 
    /// <see cref="DropCreateDatabaseIfModelChanges{DataContext}"/>
    /// </summary>
    public class DatabaseInitializer : CreateDatabaseIfNotExists<DataContext>
    {
        /// <summary>
        /// Method is used for creating some start data
        /// </summary>
        /// <param name="context">Datacontext</param>
        protected override void Seed(DataContext context)
        {
            context.Roles.Add(new ApplicationRole { Name = "admin" });
            context.Roles.Add(new ApplicationRole { Name = "user" });

            context.ProjectRoles.Add(new ProjectRole { Name = "worker" });
            context.ProjectRoles.Add(new ProjectRole { Name = "manager" });

            context.ProjectStatuses.Add(new ProjectStatus { Name = "Active" });
            context.ProjectStatuses.Add(new ProjectStatus { Name = "Completed" });
            context.ProjectStatuses.Add(new ProjectStatus { Name = "Canceled" });

            context.TaskStatuses.Add(new ProjectTaskStatus { Name = "To do" });
            context.TaskStatuses.Add(new ProjectTaskStatus { Name = "In process" });
            context.TaskStatuses.Add(new ProjectTaskStatus { Name = "Review" });
            context.TaskStatuses.Add(new ProjectTaskStatus { Name = "Done" });

            context.SaveChanges();
            base.Seed(context);
        }
    }
}
