﻿using DAL.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;

namespace DAL.EntityFramework
{
    /// <summary>
    /// Context , realize access to database
    /// <see cref="IdentityDbContext"/>
    /// <see cref="ApplicationUser"/>
    /// <see cref="DatabaseInitializer"/>
    /// </summary>
    public class DataContext : IdentityDbContext<ApplicationUser>
    {
        /// <summary>
        /// Static constructor implements database initialization
        /// </summary>
        static DataContext()
        {
            Database.SetInitializer(new DatabaseInitializer());
        }
        /// <summary>
        /// Basic constructor (need for creating migrations)
        /// </summary>
        public DataContext() : base("DefaultConnection") 
        {

        }

        /// <summary>
        /// Context constructor 
        /// </summary>
        /// <param name="connectionString"> Connection string</param>
        public DataContext(string connectionString) : base(connectionString) { }


        /// <summary>
        /// Containt set of projects and
        /// implement access 
        /// <see cref="Project"/>
        /// </summary>
        public DbSet<Project> Projects { get; set; }

        /// <summary>
        /// Containt set of project's roles and
        /// implement access
        /// <see cref="ProjectRole"/>
        /// </summary>
        public DbSet<ProjectRole> ProjectRoles { get; set; }

        /// <summary>
        /// Containt set of project's statuses and
        /// implement access
        /// <see cref="ProjectStatus"/>
        /// </summary>
        public DbSet<ProjectStatus> ProjectStatuses { get; set; }

        /// <summary>
        /// Containt set of tasks and
        /// implement access
        /// <see cref="ProjectTask"/>
        /// </summary>
        public DbSet<ProjectTask> Tasks { get; set; }

        /// <summary>
        /// Containt set of project-user table and
        /// implement access
        /// <see cref="ProjectUser"/>
        /// </summary>
        public DbSet<ProjectUser> ProjectUsers { get; set; }

        /// <summary>
        /// Containt set of task's statuses and
        /// implement access
        /// <see cref="ProjectTaskStatus"/>
        /// </summary>
        public DbSet<ProjectTaskStatus> TaskStatuses { get; set; }
    }
}
