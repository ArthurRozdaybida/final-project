﻿using DAL.EntityFramework;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace DAL.Repositories
{
    /// <summary>
    /// Repository for <see cref="ProjectTask"/> 
    /// <see cref="IRepository{ProjectTask}"/>
    /// </summary>
    public class TaskRepository : IRepository<ProjectTask>
    {
        /// <summary>
        /// DbContext <see cref="DataContext"/>
        /// </summary>
        private DataContext Database;

        /// <summary>
        /// Constructor ,which initialize <see cref="Database"/>
        /// </summary>
        /// <param name="db"></param>
        public TaskRepository(DataContext db)
        {
            Database = db;
        }

        /// <summary>
        /// Add <see cref="ProjectTask"/>
        /// </summary>
        /// <param name="item"> <see cref="ProjectTask"/></param>
        public void Add(ProjectTask item)
        {
            Database.Tasks.Add(item);
        }

        /// <summary>
        /// Delete <see cref="ProjectTask"/>
        /// </summary>
        /// <param name="id"> Id of <see cref="ProjectTask"/></param>
        public void Delete(int id)
        {
            ProjectTask task = Database.Tasks.Find(id);

            if (task != null)
                Database.Tasks.Remove(task);
        }

        /// <summary>
        /// Get <see cref="ProjectTask"/>
        /// </summary>
        /// <param name="id">Id of <see cref="ProjectTask"/></param>
        /// <returns><see cref="ProjectTask"/></returns>
        public ProjectTask Get(int id)
        {
            return Database.Tasks.Find(id);
        }

        /// <summary>
        /// Get all <see cref="ProjectTask"/>
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ProjectTask> GetAll()
        {
            return Database.Tasks.ToList();
        }

        /// <summary>
        /// Get all <see cref="ProjectTask"/> async method
        /// </summary>
        /// <returns> <see cref="IEnumerable{ProjectTask}"/></returns>
        public async Task<IEnumerable<ProjectTask>> GetAllAsync()
        {
            return await Database.Tasks.ToListAsync();
        }

        /// <summary>
        /// Get <see cref="ProjectTask"/> async method
        /// </summary>
        /// <param name="id">Id of <see cref="ProjectTask"/></param>
        /// <returns> <see cref="ProjectTask"/> </returns>
        public async Task<ProjectTask> GetAsync(int id)
        {
            return await Database.Tasks.FindAsync(id);
        }

        /// <summary>
        /// Update <see cref="ProjectTask"/>
        /// </summary>
        /// <param name="item"> <see cref="ProjectTask"/></param>
        public void Update(ProjectTask item)
        {
            Database.Set<ProjectTask>().AddOrUpdate(item);
        }
    }
}
