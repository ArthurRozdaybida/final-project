﻿using DAL.Entities;
using DAL.EntityFramework;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    /// <summary>
    /// Repository for <see cref="ProjectStatus"/>
    /// </summary>
    public class ProjectStatusRepository : IRepository<ProjectStatus>
    {
        /// <summary>
        /// DbContext <see cref="DataContext"/>
        /// </summary>
        private DataContext Database;

        /// <summary>
        /// Constructor for <see cref="Database"/> initialization
        /// </summary>
        /// <param name="db"> <see cref="DataContext"/></param>
        public ProjectStatusRepository(DataContext db)
        {
            Database = db;
        }

        /// <summary>
        /// Add <see cref="ProjectStatus"/> to database
        /// </summary>
        /// <param name="item"> <see cref="ProjectStatus"/></param>
        public void Add(ProjectStatus item)
        {
            Database.ProjectStatuses.Add(item);
        }

        /// <summary>
        /// Delete <see cref="ProjectStatus"/> from database
        /// </summary>
        /// <param name="id"> Id of <see cref="ProjectStatus"/></param>
        public void Delete(int id)
        {
            ProjectStatus projectStatus = Database.ProjectStatuses.Find(id);

            if (projectStatus != null)
                Database.ProjectStatuses.Remove(projectStatus);
        }

        /// <summary>
        /// Get <see cref="ProjectStatus"/> from database
        /// </summary>
        /// <param name="id">Id of <see cref="ProjectStatus"/></param>
        /// <returns> <see cref="ProjectStatus"/></returns>
        public ProjectStatus Get(int id)
        {
            return Database.ProjectStatuses.Find(id);
        }

        /// <summary>
        /// Get all <see cref="ProjectStatus"/>
        /// </summary>
        /// <returns> <see cref="IEnumerable{ProjectStatus}"/></returns>
        public IEnumerable<ProjectStatus> GetAll()
        {
            return Database.ProjectStatuses.ToList();
        }

        /// <summary>
        /// Get all <see cref="ProjectStatus"/> async method
        /// </summary>
        /// <returns> <see cref="IEnumerable{ProjectStatus}"/></returns>
        public async Task<IEnumerable<ProjectStatus>> GetAllAsync()
        {
            return await Database.ProjectStatuses.ToListAsync();
        }

        /// <summary>
        /// Get <see cref="ProjectStatus"/> from database async method
        /// </summary>
        /// <param name="id">Id of <see cref="ProjectStatus"/></param>
        /// <returns><see cref="ProjectStatus"/></returns>
        public async Task<ProjectStatus> GetAsync(int id)
        {
            return await Database.ProjectStatuses.FindAsync(id);
        }

        /// <summary>
        /// Update <see cref="ProjectStatus"/>
        /// </summary>
        /// <param name="item"> <see cref="ProjectStatus"/></param>
        public void Update(ProjectStatus item)
        {
            Database.Set<ProjectStatus>().AddOrUpdate(item);
        }
    }
}
