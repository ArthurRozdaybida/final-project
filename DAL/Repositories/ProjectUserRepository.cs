﻿using DAL.Entities;
using DAL.EntityFramework;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    /// <summary>
    /// Repository for <see cref="ProjectUser"/>
    /// <see cref="IRepository{ProjectUser}"/>
    /// </summary>
    public class ProjectUserRepository : IRepository<ProjectUser>
    {
        /// <summary>
        /// DbContext <see cref="DataContext"/>
        /// </summary>
        private DataContext Database;

        /// <summary>
        /// Constructor which initialize <see cref="Database"/>
        /// </summary>
        /// <param name="db"></param>
        public ProjectUserRepository(DataContext db)
        {
            Database = db;
        }

        /// <summary>
        /// Add <see cref="ProjectUser"/>
        /// </summary>
        /// <param name="item"></param>
        public void Add(ProjectUser item)
        {
            Database.ProjectUsers.Add(item);
        }

        /// <summary>
        /// Remove <see cref="ProjectUser"/>
        /// </summary>
        /// <param name="id"> Id of <see cref="ProjectUser"/></param>
        public void Delete(int id)
        {
            ProjectUser item = Database.ProjectUsers.Find(id);

            if (item != null)
                Database.ProjectUsers.Remove(item);
        }

        /// <summary>
        /// Get <see cref="ProjectUser"/>
        /// </summary>
        /// <param name="id"> Id of <see cref="ProjectUser"/></param>
        /// <returns> <see cref="ProjectUser"/></returns>
        public ProjectUser Get(int id)
        {
            return Database.ProjectUsers.Find(id);
        }

        /// <summary>
        /// Get all <see cref="ProjectUser"/>
        /// </summary>
        /// <returns> <see cref="IEnumerable{ProjectUser}"/></returns>
        public IEnumerable<ProjectUser> GetAll()
        {
            return Database.ProjectUsers.ToList();
        }

        /// <summary>
        /// Get all <see cref="ProjectUser"/> async method
        /// </summary>
        /// <returns> <see cref="IEnumerable{ProjectUser}"/></returns>
        public async Task<IEnumerable<ProjectUser>> GetAllAsync()
        {
            return await Database.ProjectUsers.ToListAsync();
        }

        /// <summary>
        /// Get <see cref="ProjectUser"/>
        /// </summary>
        /// <param name="id">Id of <see cref="ProjectUser"/></param>
        /// <returns> <see cref="ProjectUser"/></returns>
        public async Task<ProjectUser> GetAsync(int id)
        {
            return await Database.ProjectUsers.FindAsync(id);
        }

        /// <summary>
        /// Update <see cref="ProjectUser"/>
        /// </summary>
        /// <param name="item"> <see cref="ProjectUser"/></param>
        public void Update(ProjectUser item)
        {
            Database.Set<ProjectUser>().AddOrUpdate(item);
        }
    }
}
