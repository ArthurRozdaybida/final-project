﻿using DAL.Entities;
using DAL.EntityFramework;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    /// <summary>
    /// Task's statuses repository
    /// <see cref="IRepository{T}"/>
    /// </summary>
    public class TaskStatusRepository : IRepository<ProjectTaskStatus>
    {
        /// <summary>
        /// Data context, contain <see cref="DbSet{ProjectTaskStatus}"/>
        /// </summary>
        private DataContext Database;

        /// <summary>
        /// Repository constructor
        /// </summary>
        /// <param name="db"> <see cref="DataContext"/></param>
        public TaskStatusRepository(DataContext db)
        {
            this.Database = db;
        }

        /// <summary>
        /// Add task's status <see cref="ProjectTaskStatus"/> to database
        /// </summary>
        /// <param name="item"> <see cref="ProjectTaskStatus"/></param>
        public void Add(ProjectTaskStatus item)
        {
            Database.TaskStatuses.Add(item);
        }

        /// <summary>
        /// Delete <see cref="ProjectTaskStatus"/> from database
        /// </summary>
        /// <param name="id"> Id of <see cref="ProjectTaskStatus"/></param>
        public void Delete(int id)
        {
            ProjectTaskStatus status = Database.TaskStatuses.Find(id);

            if (status != null)
                Database.TaskStatuses.Remove(status);
        }

        /// <summary>
        /// Get <see cref="ProjectTaskStatus"/> from database
        /// </summary>
        /// <param name="id">Id of <see cref="ProjectTaskStatus"/></param>
        /// <returns></returns>
        public ProjectTaskStatus Get(int id)
        {
            return Database.TaskStatuses.Find(id);
        }

        /// <summary>
        /// Get all <see cref="ProjectTaskStatus"/> method
        /// </summary>
        /// <returns> <see cref="IEnumerable{ProjectTaskStatus}"/></returns>
        public IEnumerable<ProjectTaskStatus> GetAll()
        {
            return Database.TaskStatuses.ToList();
        }

        /// <summary>
        /// Get all <see cref="ProjectTaskStatus"/> async method
        /// </summary>
        /// <returns> <see cref="IEnumerable{ProjectTaskStatus}"/></returns>
        public async Task<IEnumerable<ProjectTaskStatus>> GetAllAsync()
        {
            return await Database.TaskStatuses.ToListAsync();
        }

        /// <summary>
        /// Get <see cref="ProjectTaskStatus"/> from database async method
        /// </summary>
        /// <param name="id">Id of <see cref="ProjectTaskStatus"/></param>
        /// <returns></returns>
        public async Task<ProjectTaskStatus> GetAsync(int id)
        {
            return await Database.TaskStatuses.FindAsync(id);
        }

        /// <summary>
        /// Update <see cref="ProjectTaskStatus"/> item 
        /// </summary>
        /// <param name="item"> <see cref="ProjectTaskStatus"/></param>
        public void Update(ProjectTaskStatus item)
        {
            Database.Set<ProjectTaskStatus>().AddOrUpdate(item);
        }

    }
}
