﻿using DAL.Entities;
using DAL.EntityFramework;
using DAL.Identity;
using DAL.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    /// <summary>
    /// UnitOfWork, contain repositories, <see cref="IUnitOfWork"/>
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// DbContext <see cref="DataContext"/>
        /// </summary>
        private DataContext Database;

        /// <summary>
        /// User repository
        /// </summary>
        private ApplicationUserManager userManager;

        /// <summary>
        /// Role repository
        /// </summary>
        private ApplicationRoleManager roleManager;

        /// <summary>
        /// Task's statuses repository
        /// </summary>
        private TaskStatusRepository taskStatus;

        /// <summary>
        /// Project's repository
        /// </summary>
        private ProjectRepository projectRepository;

        /// <summary>
        /// Project's role repository
        /// </summary>
        private ProjectRoleRepository projectRoleRepository;

        /// <summary>
        /// Project's status repository 
        /// </summary>
        private ProjectStatusRepository projectStatusRepository;
        /// <summary>
        /// Task repository
        /// </summary>
        private TaskRepository taskRepository;

        /// <summary>
        /// Projectuser's repository
        /// </summary>
        private ProjectUserRepository projectUserRepository;

        /// <summary>
        /// Constructor, initialize <see cref="Database"/>, <see cref="userManager"/>, <see cref="roleManager"/>
        /// </summary>
        /// <param name="connectionString"> connection string , put into <see cref="DataContext"/></param>
        public UnitOfWork(string connectionString)
        {
            Database = new DataContext(connectionString);
            userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(Database));
            roleManager = new ApplicationRoleManager(new RoleStore<ApplicationRole>(Database));
        }

        /// <summary>
        /// Property for <see cref="userManager"/>
        /// </summary>
        public ApplicationUserManager UserManager
            => userManager;

        /// <summary>
        /// Property for <see cref="roleManager"/>
        /// </summary>
        public ApplicationRoleManager RoleManager
            => roleManager;

        /// <summary>
        /// Property for <see cref="taskStatus"/>
        /// </summary>
        public TaskStatusRepository TaskStatus
        {
            get
            {
                if (taskStatus is null)
                    taskStatus = new TaskStatusRepository(Database);
                return taskStatus;
            }
        }

        /// <summary>
        /// Property for <see cref="projectRepository"/>
        /// </summary>
        public ProjectRepository Project
        {
            get
            {
                if (projectRepository is null)
                    projectRepository = new ProjectRepository(Database);
                return projectRepository;
            }
        }

        /// <summary>
        /// Property for <see cref="projectRoleRepository"/>
        /// </summary>
        public ProjectRoleRepository ProjectRole
        {
            get
            {
                if (projectRoleRepository is null)
                    projectRoleRepository = new ProjectRoleRepository(Database);
                return projectRoleRepository;
            }
        }

        /// <summary>
        /// Property for <see cref="projectStatusRepository"/>
        /// </summary>
        public ProjectStatusRepository ProjectStatus
        {
            get
            {
                if (projectStatusRepository is null)
                    projectStatusRepository = new ProjectStatusRepository(Database);
                return projectStatusRepository;
            }
        }

        /// <summary>
        /// Property for <see cref="taskRepository"/>
        /// </summary>
        public TaskRepository Task
        {
            get
            {
                if (taskRepository is null)
                    taskRepository = new TaskRepository(Database);
                return taskRepository;
            }
        }
        
        /// <summary>
        /// Property for <see cref="projectUserRepository"/> 
        /// </summary>
        public ProjectUserRepository ProjectUser
        {
            get
            {
                if (projectUserRepository is null)
                    projectUserRepository = new ProjectUserRepository(Database);
                return projectUserRepository;
            }
        }

        /// <summary>
        /// Save database's changes method
        /// </summary>
        public void Save()
        {
            Database.SaveChanges();
        }

        /// <summary>
        /// Save database's changes async method
        /// </summary>
        /// <returns></returns>
        public async Task SaveAsync()
        {
            await Database.SaveChangesAsync();
        }

        /// <summary>
        /// Dispose method 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private bool disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    userManager.Dispose();
                    roleManager.Dispose();
                    Database.Dispose();
                }
                this.disposed = true;
            }
        }
    }
}
