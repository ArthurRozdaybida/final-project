﻿using DAL.EntityFramework;
using DAL.Interfaces;
using DAL.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq;
using System.Data.Entity.Migrations;

namespace DAL.Repositories
{
    /// <summary>
    /// Project's role repository
    /// <see cref="IRepository{ProjectRole}"/>
    /// </summary>
    public class ProjectRoleRepository : IRepository<ProjectRole>
    {
        /// <summary>
        /// Context of <see cref="DataContext"/>
        /// </summary>
        private DataContext Database;

        /// <summary>
        /// Contructor which initialize <see cref="DataContext"/>
        /// </summary>
        /// <param name="db"> <see cref="DataContext"/></param>
        public ProjectRoleRepository(DataContext db)
        {
            Database = db;
        }

        /// <summary>
        /// Add <see cref="ProjectRole"/> to database
        /// </summary>
        /// <param name="item"> <see cref="ProjectRole"/></param>
        public void Add(ProjectRole item)
        {
            Database.ProjectRoles.Add(item);
        }

        /// <summary>
        /// Delete <see cref="ProjectRole"/> from database
        /// </summary>
        /// <param name="id"> Id of <see cref="ProjectRole"/></param>
        public void Delete(int id)
        {
            ProjectRole role = Database.ProjectRoles.Find(id);

            if (role != null)
                Database.ProjectRoles.Remove(role);
        }

        /// <summary>
        /// Get <see cref="ProjectRole"/> method
        /// </summary>
        /// <param name="id">Id of <see cref="ProjectRole"/></param>
        /// <returns> <see cref="ProjectRole"/></returns>
        public ProjectRole Get(int id)
        {
            return Database.ProjectRoles.Find(id);
        }

        /// <summary>
        /// Get all <see cref="ProjectRole"/> from database method
        /// </summary>
        /// <returns> <see cref="IEnumerable{ProjectRole}"/></returns>
        public IEnumerable<ProjectRole> GetAll()
        {
            return Database.ProjectRoles.ToList();
        }

        /// <summary>
        /// Get all <see cref="ProjectRole"/> from database async method
        /// </summary>
        /// <returns> <see cref="IEnumerable{ProjectRole}"/></returns>
        public async Task<IEnumerable<ProjectRole>> GetAllAsync()
        {
            return await Database.ProjectRoles.ToListAsync();
        }

        /// <summary>
        /// Get <see cref="ProjectRole"/> async method
        /// </summary>
        /// <param name="id">Id of <see cref="ProjectRole"/></param>
        /// <returns></returns>
        public async Task<ProjectRole> GetAsync(int id)
        {
            return await Database.ProjectRoles.FindAsync(id);
        }

        /// <summary>
        /// Update <see cref="ProjectRole"/>
        /// </summary>
        /// <param name="item"></param>
        public void Update(ProjectRole item)
        {
            Database.Set<ProjectRole>().AddOrUpdate(item);
        }
    }
}
