﻿using DAL.EntityFramework;
using DAL.Entities;
using DAL.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace DAL.Repositories
{
    /// <summary>
    /// Project's repository, implement crud operations for <see cref="Project"/>
    /// <see cref="IRepository{Project}"/>
    /// </summary>
    public class ProjectRepository : IRepository<Project>
    {
        /// <summary>
        /// DbContext , which contain <see cref="DbSet{Project}"/>
        /// </summary>
        private DataContext Database;

        /// <summary>
        /// Constructor , which initialize <see cref="DataContext"/>
        /// </summary>
        /// <param name="db"></param>
        public ProjectRepository(DataContext db)
        {
            Database = db;
        }

        /// <summary>
        /// Add <see cref="Project"/> to database
        /// </summary>
        /// <param name="item"> <see cref="Project"/> </param>
        public void Add(Project item)
        {
            Database.Projects.Add(item);
        }

        /// <summary>
        /// Delete <see cref="Project"/> from database
        /// </summary>
        /// <param name="id">Id of <see cref="Project"/></param>
        public void Delete(int id)
        {
            Project project = Database.Projects.Find(id);

            if (project != null)
                Database.Projects.Remove(project);
        }

        /// <summary>
        /// Get <see cref="Project"/> from database
        /// </summary>
        /// <param name="id">Id of <see cref="Project"/></param>
        /// <returns> <see cref="Project"/></returns>
        public Project Get(int id)
        {
            return Database.Projects.Find(id);
        }

        /// <summary>
        /// Get all <see cref="Project"/>
        /// </summary>
        /// <returns> <see cref="IEnumerable{Project}"/></returns>
        public IEnumerable<Project> GetAll()
        {
            return Database.Projects.ToList();
        }

        /// <summary>
        /// Get all <see cref="Project"/> async method
        /// </summary>
        /// <returns><see cref="IEnumerable{Project}"/></returns>
        public async Task<IEnumerable<Project>> GetAllAsync()
        {
            return await Database.Projects.ToListAsync();
        }

        /// <summary>
        /// Get <see cref="Project"/> async method
        /// </summary>
        /// <param name="id">Id of <see cref="Project"/></param>
        /// <returns> <see cref="Project"/></returns>
        public async Task<Project> GetAsync(int id)
        {
            return await Database.Projects.FindAsync(id);
        }

        /// <summary>
        /// Update <see cref="Project"/>
        /// </summary>
        /// <param name="item"> <see cref="Project"/> </param>
        public void Update(Project item)
        {
            Database.Set<Project>().AddOrUpdate(item);
        }
    }
}
