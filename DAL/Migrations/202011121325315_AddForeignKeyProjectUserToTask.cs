﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForeignKeyProjectUserToTask : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.TaskStatus", newName: "ProjectTaskStatus");
            AddColumn("dbo.ProjectTasks", "ProjectUserId", c => c.Int(nullable: false));
            CreateIndex("dbo.ProjectTasks", "ProjectUserId");
            AddForeignKey("dbo.ProjectTasks", "ProjectUserId", "dbo.ProjectUsers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProjectTasks", "ProjectUserId", "dbo.ProjectUsers");
            DropIndex("dbo.ProjectTasks", new[] { "ProjectUserId" });
            DropColumn("dbo.ProjectTasks", "ProjectUserId");
            RenameTable(name: "dbo.ProjectTaskStatus", newName: "TaskStatus");
        }
    }
}
