﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    /// <summary>
    /// Represent current stutus of task
    /// to present state of task
    /// </summary>
    public class ProjectTaskStatus
    {
        /// <summary>
        /// Unique fild , to find instance
        /// </summary>
        [Key]
        [Required]
        public int Id { get; set; }
        /// <summary>
        /// Name of status 
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}
