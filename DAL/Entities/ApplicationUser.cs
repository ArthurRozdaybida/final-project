﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace DAL.Entities
{
    /// <summary>
    /// Represent user 
    /// <see cref="IdentityUser"/>
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// Name of user (contain firstname and secondname,
        /// this level of decomposition is enough)
        /// </summary>
        public string Name { get; set; }
    }
}
