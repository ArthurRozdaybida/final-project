﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
    /// <summary>
    /// This class represent that certain user is a member of project team
    /// This class contain project, user and role ids 
    /// and decides relationship many to many between project user
    /// and represent role to determine user's access level
    /// <see cref="ApplicationUser"/>
    /// <see cref="DAL.Entities.Project"/>
    /// <see cref="ProjectRole"/>
    /// </summary>
    public class ProjectUser
    {
        /// <summary>
        /// Unique filed, to find instance
        /// </summary>
        [Key]
        [Required]
        public int Id { get; set; }
       
        /// <summary>
        /// Id of project 
        /// <see cref="Project"/>
        /// </summary>
        [Required]
        public int ProjectId { get; set; }
        /// <summary>
        /// Project field
        /// </summary>
        [ForeignKey("ProjectId")]       
        public Project Project { get; set; }

        /// <summary>
        /// Id of <see cref="ApplicationUser"/> 
        /// </summary>
        [Required]
        public string UserId { get; set; }
        /// <summary>
        /// ApplicationUser field
        /// </summary>
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }

        /// <summary>
        /// Id of <see cref="Project"/>
        /// </summary>
        [Required]
        public int RoleId { get; set; }
        /// <summary>
        /// Role field
        /// </summary>
        [ForeignKey("RoleId")]
        public ProjectRole Role { get; set; }

    }
}
