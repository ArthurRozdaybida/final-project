﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
    /// <summary>
    /// Represent project 
    /// <see cref="ProjectStatus"/>
    /// </summary>
    public class Project
    {
        /// <summary>
        /// Unique field , to find instance
        /// </summary>
        [Key]
        [Required]
        public int Id { get; set; }
        /// <summary>
        /// Name of project
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Project status 
        /// <see cref="ProjectStatus"/>
        /// </summary>
        [Required]
        public int StatusId { get; set; }

        /// <summary>
        /// Status of project
        /// </summary>
        [ForeignKey("StatusId")]
        public ProjectStatus Status { get; set; }

        /// <summary>
        /// Link to repository or another source 
        /// </summary>
        [MinLength(7)]
        public string Link { get; set; }

    }
}
