﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    /// <summary>
    /// Represent status of the project
    /// to present current state of project
    /// </summary>
    public class ProjectStatus
    {
        /// <summary>
        /// Unique field , to find instance
        /// </summary>
        [Key]
        [Required]
        public int Id { get; set; }
        /// <summary>
        /// Name of project status
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}
