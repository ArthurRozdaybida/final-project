﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.Entities
{
    /// <summary>
    /// Represent task of some project
    /// <see cref="ProjectTaskStatus"/>
    /// </summary>
    public class ProjectTask
    {
        /// <summary>
        /// Unique field , to find instance
        /// </summary>
        [Key]
        [Required]
        public int Id { get; set; }

        /// <summary>
        ///Name of task status 
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// Id of status
        /// <see cref="ProjectTaskStatus"/>
        /// </summary>
        [Required]
        public int StatusId { get; set; }
        /// <summary>
        /// Status of task
        /// </summary>
        [ForeignKey("StatusId")]
        public ProjectTaskStatus Status { get; set; }
        
        /// <summary>
        /// Task's deadline
        /// </summary>
        [Required]
        public DateTime Deadline {get;set;}

        /// <summary>
        /// Id of <see cref="DAL.Entities.ProjectUser"/>
        /// </summary>
        [Required]
        public int ProjectUserId { get; set; }

        /// <summary>
        /// <see cref="DAL.Entities.ProjectUser"/>
        /// </summary>
        [ForeignKey("ProjectUserId")]
        public ProjectUser ProjectUser { get; set; }
    }
}
