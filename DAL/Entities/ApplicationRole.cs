﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace DAL.Entities
{
    /// <summary>
    /// Represent role of user 
    /// <see cref="IdentityRole"/>
    /// </summary>
    public class ApplicationRole : IdentityRole
    {
    }
}
