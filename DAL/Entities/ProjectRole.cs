﻿using System.ComponentModel.DataAnnotations;

namespace DAL.Entities
{
    /// <summary>
    /// This class repreesents user's role in project level
    /// </summary>
    public class ProjectRole
    {
        /// <summary>
        /// Unique field, to find instance
        /// </summary>
        [Key]
        [Required]
        public int Id { get; set; }
        /// <summary>
        /// Name of project role
        /// </summary>
        [Required]
        public string Name { get; set; }
    }
}
