﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    /// <summary>
    /// Inteface for basic repository
    /// </summary>
    /// <typeparam name="T"> One of the <see cref="DAL.Entities"/></typeparam>
    public interface IRepository<T> where T: class
    {
        /// <summary>
        /// Item getter
        /// </summary>
        /// <param name="id"> Id of item</param>
        /// <returns></returns>
        T Get(int id);
        
        /// <summary>
        /// Item getter async method
        /// </summary>
        /// <param name="id">Id of item</param>
        /// <returns></returns>
        Task<T> GetAsync(int id);

        /// <summary>
        /// All method getter
        /// </summary>
        /// <returns></returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// All method getter async method
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Add item to database method
        /// </summary>
        /// <param name="item"></param>
        void Add(T item);

        /// <summary>
        /// Delete item from database 
        /// </summary>
        /// <param name="id">Id of item</param>
        void Delete(int id);

        /// <summary>
        /// Update item 
        /// </summary>
        /// <param name="item"></param>
        void Update(T item);

    }
}
