﻿using DAL.Entities;
using DAL.Identity;
using DAL.Repositories;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    /// <summary>
    /// IUnitOfWork interface
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// User repository
        /// </summary>
        ApplicationUserManager UserManager { get; }

        /// <summary>
        /// Role repository
        /// </summary>
        ApplicationRoleManager RoleManager { get; }

        /// <summary>
        /// Task's statuses repository
        /// </summary>
        TaskStatusRepository TaskStatus { get; }

        /// <summary>
        /// Project's repository
        /// </summary>
        ProjectRepository Project { get; }

        /// <summary>
        /// Project's role repository
        /// </summary>
        ProjectRoleRepository ProjectRole { get; }

        /// <summary>
        /// Project's status repository
        /// </summary>
        ProjectStatusRepository ProjectStatus { get; }
        
        /// <summary>
        /// Task repository
        /// </summary>
        TaskRepository Task { get; }
        
        /// <summary>
        /// ProjectUser reposiroty 
        /// </summary>
        ProjectUserRepository ProjectUser { get; }

        /// <summary>
        /// Save changes async method
        /// </summary>
        /// <returns></returns>
        Task SaveAsync();

        /// <summary>
        /// Save changes method
        /// </summary>
        void Save();

        /// <summary>
        /// Dispose method
        /// </summary>
        void Dispose();
    }
}
