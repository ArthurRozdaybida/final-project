﻿using DAL.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DAL.Identity
{
    /// <summary>
    /// Application role manager 
    /// Represent user's role repository
    /// </summary>
    /// <see cref="RoleManager{ApplicationRole}"/>
    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        /// <summary>
        /// Constructor of role's repository
        /// </summary>
        /// <param name="store"></param>
        public ApplicationRoleManager(RoleStore<ApplicationRole> store)
            : base(store)
        {
        }
    }
}
