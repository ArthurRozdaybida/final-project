﻿using DAL.Entities;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Identity
{
    /// <summary>
    /// Repository for table aspnetuser
    /// </summary>
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        /// <summary>
        /// Constructor ApplicationUserManager
        /// </summary>
        /// <param name="store"></param>
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        /// <summary>
        /// Get method for users
        /// </summary>
        /// <param name="id"> id of <see cref="ApplicationUser"/></param>
        /// <returns> <see cref="ApplicationUser"/></returns>
        public ApplicationUser Get(string id)
        {
            return this.FindById(id);
        }

        /// <summary>
        /// Async get method for users
        /// </summary>
        /// <param name="id">id of <see cref="ApplicationUser"/></param>
        /// <returns> <see cref="ApplicationUser"/></returns>
        public async Task<ApplicationUser> GetAsync(string id)
        {
            return await this.FindByIdAsync(id);
        }
        /// <summary>
        /// Get all users method
        /// </summary>
        /// <returns> <see cref="IEnumerable{ApplicationUser}"/></returns>
        public IEnumerable<ApplicationUser> GetAll()
        {
            return this.Users.ToList();
        }
        /// <summary>
        /// Get all users async method
        /// </summary>
        /// <returns> <see cref="IEnumerable{ApplicationUser}"/></returns>
        public async Task<IEnumerable<ApplicationUser>> GetAllAsync()
        {
            return await this.Users.ToListAsync();
        }

        /// <summary>
        /// Delete user method
        /// </summary>
        /// <param name="id"> Id of <see cref="ApplicationUser"/></param>
        public void Delete(string id)
        {
            ApplicationUser user = this.FindById(id);

            if(user != null)
            {
                this.Delete(user);
            }
        }

        /// <summary>
        /// Delete user async method
        /// </summary>
        /// <param name="id">Id of <see cref="ApplicationUser"/></param>
        /// <returns></returns>
        public async Task DeleteAsync(string id)
        {
            ApplicationUser user = await this.FindByIdAsync(id);

            if(user != null)
            {
                await this.DeleteAsync(user);
            }
        }

        
    }
}
