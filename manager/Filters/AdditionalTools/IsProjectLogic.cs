﻿using BLL.Interfaces;
using System;
using System.Threading.Tasks;

namespace manager.Filters.AdditionalTools
{
    /// <summary>
    /// Implement logic for givin access for users to special actions
    /// </summary>
    public class IsProjectLogic
    {
        /// <summary>
        /// Give access to service
        /// </summary>
        private IService service;

        /// <summary>
        /// DI set value to <see cref="IService"/>
        /// </summary>
        /// <param name="Service"></param>
        public IsProjectLogic(IService Service)
        {
            service = Service;
        }

        /// <summary>
        /// Return bool value , is current user in role manager
        /// </summary>
        /// <param name="projectId">Project's Id</param>
        /// <param name="userId">User's Id</param>
        /// <returns></returns>
        public async Task IsManager(int projectId, string userId)
        {
            var projectUser = await service.ProjectUser
                .GetByUserAndProjectAsync(userId, projectId);

            if (projectUser.RoleId == 1)
                throw new Exception();
        }

        /// <summary>
        /// Returns bool value can user invoke with this method
        /// </summary>
        /// <param name="taskId">Task's Id</param>
        /// <param name="userId">User's Id</param>
        /// <returns></returns>
        public async Task HasAccess(int taskId, string userId)
        {
            var task = await service.Task.GetAsync(taskId);
            var projectUser = await service.ProjectUser.GetAsync(task.ProjectUserId);

            var projectUserRole = await service.ProjectUser
                .GetByUserAndProjectAsync(userId, projectUser.ProjectId);

            if(projectUserRole.RoleId == 1)
            {
                if (projectUser.UserId != userId)
                    throw new Exception();
            }
            if(projectUser.UserId != userId)
            {
                if (projectUserRole.RoleId == 1)
                    throw new Exception();
            }
        }
    }
}