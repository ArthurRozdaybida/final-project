﻿using System;
using System.Web.Mvc;

namespace manager.Filters
{
    /// <summary>
    /// Exeption filter
    /// </summary>
    public class HasAccess : FilterAttribute, IExceptionFilter
    {
        /// <summary>
        /// Redirect user if catch exeption
        /// </summary>
        /// <param name="filterContext"></param>
        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled && filterContext.Exception is Exception)
            {
                filterContext.Result = new RedirectResult("/");
                filterContext.ExceptionHandled = true;
            }
        }
    }
}