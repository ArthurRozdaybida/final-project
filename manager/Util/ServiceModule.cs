﻿using BLL.Interfaces;
using BLL.Services;
using Ninject.Modules;

namespace manager.Util
{
    /// <summary>
    /// DI module fo <see cref="Service"/>
    /// </summary>
    public class ServiceModule : NinjectModule
    {
        /// <summary>
        /// Create DI
        /// </summary>
        public override void Load()
        {
            Bind<IService>().To<Service>();
            Bind<IUserService>().To<UserService>();
        }
    }

}