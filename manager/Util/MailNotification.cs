﻿using System.Net;
using System.Net.Mail;

namespace manager.Util
{
    /// <summary>
    /// Mail notification , say for user that he was  added to project
    /// </summary>
    public class MailNotification
    {
        /// <summary>
        /// Send email letter
        /// </summary>
        /// <param name="address"></param>
        /// <param name="projectName"></param>
        /// <param name="name"></param>
        public static void GetNotification(string address, string projectName, string name)
        {
            MailMessage message = new MailMessage();

            message.Subject = "MANAGER UP, project invite";
            message.Body = $"Dear {name}, you have been added to project : {projectName}";
            message.From = new MailAddress("hmarochossss@gmail.com");
            message.To.Add(address);
            message.IsBodyHtml = false;


            SmtpClient smtp = new SmtpClient("smtp.gmail.com");
            smtp.Port = 587;
            smtp.UseDefaultCredentials = true;
            smtp.EnableSsl = true;
            smtp.Credentials = new NetworkCredential("hmarochossss@gmail.com", "arty03042001");

            smtp.Send(message);
        }
    }
}