﻿using BLL.DTO;
using manager.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace manager.ViewModels
{
    /// <summary>
    /// Add task view model, using for form
    /// </summary>
    public class AddTaskViewModel
    {
        /// <summary>
        /// Task's name
        /// </summary>
        [Required(ErrorMessage = "Please input task's name")]
        [MinLength(4, ErrorMessage ="Name's lenght must be more than 4")]
        [MaxLength(40, ErrorMessage ="Name's length must be less than 40")]
        public string Name { get; set; }

        /// <summary>
        /// Id of choosen user
        /// </summary>
        public string  UserId { get; set; }

        /// <summary>
        /// Project's id
        /// </summary>
        public int ProjectId { get; set; }

        /// <summary>
        /// Id of status
        /// </summary>
        public int StatusId { get; set; }

        /// <summary>
        /// Task's deadline
        /// </summary>
        [DataType(DataType.Date)]
        [FutureDate(ErrorMessage = "Please choose future date")]
        public DateTime Deadline { get; set; }

        /// <summary>
        /// Get all project's members
        /// </summary>
        public IEnumerable<UserDTO> GetMembers { get; set; }
    }
}