﻿using manager.Validators;
using System.ComponentModel.DataAnnotations;

namespace manager.ViewModels
{
    /// <summary>
    /// Viewmodel for creating project
    /// </summary>
    public class CreateProjectViewModel
    {
        /// <summary>
        /// Project's name
        /// </summary>
        [Required(ErrorMessage = "Please input project name")]
        [MinLength(4, ErrorMessage = "Name's lenght must be more than 4")]
        [MaxLength(40, ErrorMessage = "Name's length must be less than 40")]
        public string Name{get;set;}

        /// <summary>
        /// Link to repository of another source
        /// </summary>
        [Required(ErrorMessage = "Please input link")]
        [MinLength(5, ErrorMessage = "Name's lenght must be more than 4")]
        public string Link { get; set; }
    }
}