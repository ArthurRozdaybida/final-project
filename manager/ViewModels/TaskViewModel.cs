﻿using System;
using System.ComponentModel.DataAnnotations;

namespace manager.ViewModels
{
    /// <summary>
    /// Task's view model
    /// </summary>
    public class TaskViewModel
    {
        /// <summary>
        /// Task's id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Task's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Task's deadline
        /// </summary>
        [DataType(DataType.Date)]
        public DateTime Deadline { get; set;}

        /// <summary>
        /// Task's status
        /// </summary>
        public string Status { get; set; }
    }
}