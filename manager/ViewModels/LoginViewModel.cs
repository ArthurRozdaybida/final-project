﻿using System.ComponentModel.DataAnnotations;

namespace manager.ViewModels
{
    /// <summary>
    /// View model for autorization
    /// </summary>
    public class LoginViewModel
    {
        /// <summary>
        /// User's username
        /// </summary>
        [Required(ErrorMessage = "Please enter your username")]
        public string Username { get; set; }

        /// <summary>
        /// User's password
        /// </summary>
        [Required(ErrorMessage = "Please enter your password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }

}