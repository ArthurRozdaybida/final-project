﻿namespace manager.ViewModels
{
    /// <summary>
    /// View model for user view
    /// </summary>
    public class MemberViewModel
    {
        /// <summary>
        /// User's id
        /// </summary>
        public string  Id { get; set; }
        
        /// <summary>
        /// User's username
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// User's role in current project
        /// </summary>
        public string ProjectRole { get; set; }

        /// <summary>
        /// User's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// User's email
        /// </summary>
        public string Email { get; set; }
    }
}