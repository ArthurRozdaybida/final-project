﻿using BLL.DTO;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace manager.ViewModels
{

    /// <summary>
    /// View model for adding new project member form
    /// </summary>
    public class AddMembersViewModel
    {
        /// <summary>
        /// Return list which contain all users
        /// </summary>
        public List<UserDTO> GetUsers { get; set; }

        /// <summary>
        /// List of choosen users
        /// </summary>
        [Required(ErrorMessage = "Choose new project's members")]
        public IEnumerable<string> Users { get; set; }

        /// <summary>
        /// List of all users
        /// </summary>
        public IEnumerable<ProjectRoleDTO> GetRoles {get;set;}

        /// <summary>
        /// Choosen role
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        /// Project's Id
        /// </summary>
        public int ProjectId { get; set; }
    }
}