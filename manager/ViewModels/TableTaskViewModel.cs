﻿using BLL.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace manager.ViewModels
{
    /// <summary>
    /// Task's view model for table
    /// </summary>
    public class TableTaskViewModel
    {
        /// <summary>
        /// Task's id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Task's deadline
        /// </summary>
        [DataType(DataType.Date)]
        public DateTime Deadline { get; set; }

        /// <summary>
        /// Task's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// User, use for comfortable access to user's data
        /// </summary>
        public UserDTO User { get; set; }

        /// <summary>
        /// Task's status
        /// </summary>
        public string Status { get; set; }
    }
}