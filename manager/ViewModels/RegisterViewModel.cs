﻿using System.ComponentModel.DataAnnotations;

namespace manager.ViewModels
{
    /// <summary>
    /// View model for registration
    /// </summary>
    public class RegisterViewModel
    {
        /// <summary>
        /// User's email
        /// </summary>
        [Required(ErrorMessage = "Input email")]
        public string Email { get; set; }

        /// <summary>
        /// User's password
        /// </summary>
        [Required(ErrorMessage = "Input password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        /// <summary>
        /// Password's confirmation
        /// </summary>
        [Required(ErrorMessage = "Input confirm password")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// User's name
        /// </summary>
        [Required(ErrorMessage = "Input name")]
        public string Name { get; set; }

        /// <summary>
        /// User's username
        /// </summary>
        [Required(ErrorMessage = "Input username")]
        public string UserName { get; set; }
    }

}