﻿namespace manager.ViewModels
{
    /// <summary>
    /// Project's view model
    /// </summary>
    public class ProjectViewModel
    {
        /// <summary>
        /// Project's view model
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Project's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Project's link to some sources
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Project's status
        /// </summary>
        public string StatusName { get; set; }

        /// <summary>
        /// Project's progress in procents
        /// </summary>
        public double Percent { get; set; }

    }
}