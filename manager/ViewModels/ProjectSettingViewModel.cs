﻿using BLL.DTO;
using System.Collections.Generic;

namespace manager.ViewModels
{
    /// <summary>
    /// Project's setting view model
    /// </summary>
    public class ProjectSettingViewModel
    {
        /// <summary>
        /// Project's id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Project's name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Project's status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Project's progress 
        /// </summary>
        public double Percent { get; set; }

        /// <summary>
        /// List of project's members
        /// </summary>
        public IEnumerable<MemberViewModel> Members { get; set; }

        /// <summary>
        /// List of project's task
        /// </summary>
        public IEnumerable<TableTaskViewModel> Tasks { get; set; }
    }
}