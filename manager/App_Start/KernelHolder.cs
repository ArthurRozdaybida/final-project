﻿using BLL.Infrastructure;
using BLL.Interfaces;
using manager.Util;
using Ninject;
using Ninject.Modules;


namespace manager.App_Start
{
    /// <summary>
    /// Create DI modules and getter for <see cref="IUserService"/>
    /// </summary>
    public static class KernelHolder
    {
        /// <summary>
        /// Kernel of DI modules
        /// </summary>
        static StandardKernel kernel;

        /// <summary>
        /// Property for <see cref="kernel"/>
        /// </summary>
        public static StandardKernel Kernel
        {
            get
            {
                if (kernel == null)
                {
                    NinjectModule dataModule = new DataModule("DefaultConnection");
                    NinjectModule serviceModule = new ServiceModule();

                    kernel = new StandardKernel(serviceModule, dataModule);
                }
                return kernel;
            }
        }

        /// <summary>
        /// Create and return <see cref="IUserService"/>
        /// Use for getting OwinContext
        /// </summary>
        /// <returns></returns>
        public static IUserService CreateUserService()
        {
            return KernelHolder.Kernel.Get<IUserService>();
        }
    }

}