﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using manager.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace manager.Controllers
{
    /// <summary>
    /// Admin controller , give access to admin's method
    /// </summary>
    [Authorize(Roles = "admin")]
    public class AdminController : Controller
    {
        /// <summary>
        /// Give access to services
        /// </summary>
        private IService service;

        /// <summary>
        /// Constructor , seet value to <see cref="service"/>
        /// </summary>
        /// <param name="service"></param>
        public AdminController(IService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Index page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index() 
            => View();

        /// <summary>
        /// Returns page with all users
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> AllUsers()
        {
            var users = await service.User.GetAllAsync();

            return View(users);
        }

        /// <summary>
        /// Returns page with all projects
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> AllProjects()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ProjectDTO, ProjectViewModel>()
            .ForMember("StatusName", item
            => item.MapFrom(x => service.ProjectStatus.Get(x.StatusId).Name)));

            var mapper = new Mapper(config);

            var projects = mapper.Map<IEnumerable<ProjectDTO>, 
                IEnumerable<ProjectViewModel>>(await service.Project.GetAllAsync());

            return View(projects);
        }

        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="Id">Id of <see cref="UserDTO"/></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteUser(string Id)
        {
            await service.User.DeleteAsync(Id);

            return RedirectToAction("AllUsers", "Admin");
        }

        /// <summary>
        /// Change <see cref="UserDTO"/> role
        /// </summary>
        /// <param name="Id">Id of <see cref="ProjectDTO"/></param>
        /// <param name="Name">Name of role</param>
        /// <returns></returns>
        public async Task<ActionResult> ChangeRole(string Id, string Name)
        {
            await service.User.ChangeUserRole(Id, Name);

            return RedirectToAction("AllUsers", "Admin");
        }

        /// <summary>
        /// Delete project
        /// </summary>
        /// <param name="Id">id of <see cref="ProjectDTO"/></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteProject(int Id)
        {
            await service.Project.Delete(Id);

            return RedirectToAction("AllProjects", "Admin");
        }
    }
}