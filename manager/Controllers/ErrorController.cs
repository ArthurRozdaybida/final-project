﻿using System.Web.Mvc;

namespace manager.Controllers
{
    /// <summary>
    /// Controller for error handing
    /// </summary>
    public class ErrorController : Controller
    {
        /// <summary>
        /// Redirect to 404 error page
        /// </summary>
        /// <returns></returns>
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;

            return View();
        }

        /// <summary>
        /// Redirect to 500 error page
        /// </summary>
        /// <returns></returns>
        public ActionResult InternalServerError()
        {
            Response.StatusCode = 500;
            return View();
        }
    }
}