﻿using BLL.DTO;
using BLL.Interfaces;
using manager.ViewModels;
using Microsoft.AspNet.Identity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AutoMapper;
using System.Collections.Generic;
using manager.Filters.AdditionalTools;
using manager.Filters;
using manager.Util;

namespace manager.Controllers
{
    /// <summary>
    /// Controller for projects actions
    /// </summary>
    [Authorize]
    public class ProjectController : Controller
    {
        /// <summary>
        /// <see cref="IService"/> give access to services
        /// </summary>
        private IService service;

        /// <summary>
        /// Use to invoke exception
        /// </summary>
        private IsProjectLogic filterManager;

        /// <summary>
        /// Basic constructor 
        /// </summary>
        /// <param name="service"> DI put this </param>
        public ProjectController(IService service)
        {
            this.service = service;
            filterManager = new IsProjectLogic(service);
        }

        /// <summary>
        /// Add project httpget action
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public ActionResult AddProject()
        {
            return View();
        }

        /// <summary>
        /// Add project httppost action
        /// </summary>
        /// <param name="model"> <see cref="CreateProjectViewModel"/></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public async Task<ActionResult> AddProject(CreateProjectViewModel model)
        {
            if (ModelState.IsValid)
            {
                await service.Project.Add(new ProjectDTO()
                {
                    Name = model.Name,
                    Link = model.Link,
                    StatusId = 1
                });

                var project = await service.Project.GetAllAsync();
                await service.ProjectUser.Add(new ProjectUserDTO
                {
                    UserId = User.Identity.GetUserId(),
                    ProjectId = project.LastOrDefault().Id,
                    RoleId = 2
                });

                return RedirectToAction("Index", "Home");
            }

            return View("AddProject");
        }

        /// <summary>
        /// Project action , go to task table and basic tools
        /// </summary>
        /// <param name="Id">Id of <see cref="ProjectDTO"/></param>
        /// <returns></returns>
        public async Task<ActionResult> Project(int Id)
        {
            var project = await service.Project.GetAsync(Id);
            var tasks = await service.Task.GetProjectTask(Id);

            var config = new MapperConfiguration(cfg => cfg.CreateMap<TaskDTO, TableTaskViewModel>()
            .ForMember("Status", c => c.MapFrom(item => service.TaskStatus.Get(item.StatusId).Name))
            .ForMember("User", c => c.MapFrom(item
             => service.User.Get(service.ProjectUser.Get(item.ProjectUserId).UserId))));

            var mapper = new Mapper(config);
            var projectTasks = mapper.Map<IEnumerable<TaskDTO>, IEnumerable<TableTaskViewModel>>(tasks);

            var todo = projectTasks.Where(item => item.Status == "To do");
            var process = projectTasks.Where(item => item.Status == "In process");
            var review = projectTasks.Where(item => item.Status == "Review");
            var done = projectTasks.Where(item => item.Status == "Done");

            #region ViewbagRegion

            ViewBag.todo = todo;
            ViewBag.process = process;
            ViewBag.review = review;
            ViewBag.done = done;
            ViewBag.name = project.Name;
            ViewBag.link = project.Link;
            ViewBag.id = Id;
            ViewBag.progress = (int)(service.Project.GetProgress(Id) * 100);

            #endregion

            return View();
        }

        /// <summary>
        /// Giva membres list and tool(add new users) with notification
        /// </summary>
        /// <param name="Id">Id of <see cref="ProjectDTO"/></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult> Members(int Id)
        {
            var config = new MapperConfiguration(cfg
                => cfg.CreateMap<UserDTO, MemberViewModel>()
                .ForMember("ProjectRole", item => item.MapFrom(c
                => service.ProjectUser.GetProjectRole(c.Id, Id).Name)));

            var mapper = new Mapper(config);
            var members = mapper.Map<IEnumerable<UserDTO>,
                IEnumerable<MemberViewModel>>(await service.Project.GetMembersAsync(Id));

            ViewBag.members = members;
            ViewBag.id = Id;

            var users = await service.User.GetFreeMembers(Id);
            var roles = await service.ProjectRole.GetAllAsync();
            AddMembersViewModel model = new AddMembersViewModel() { GetRoles = roles, GetUsers = users.ToList() };

            return View(model);
        }

        /// <summary>
        /// Post method for members page
        /// </summary>
        /// <param name="model"> <see cref="AddMembersViewModel"/></param>
        /// <returns></returns>
        [HttpPost]
        [IsManagerFilter]
        public async Task<ActionResult> Members(AddMembersViewModel model)
        {
            await filterManager.IsManager(model.ProjectId, User.Identity.GetUserId());

            foreach (string item in model.Users)
            {
                var user = await service.User.GetAsync(item);
                var project = await service.Project.GetAsync(model.ProjectId);

                MailNotification.GetNotification(user.Email, project.Name, user.Name);

                await service.ProjectUser.Add(new ProjectUserDTO
                {
                    ProjectId = model.ProjectId,
                    RoleId = int.Parse(model.Role),
                    UserId = item
                });


            }

            var config = new MapperConfiguration(cfg
                => cfg.CreateMap<UserDTO, MemberViewModel>()
                .ForMember("ProjectRole", item => item.MapFrom(c
                => service.ProjectUser.GetProjectRole(c.Id, model.ProjectId).Name)));

            var mapper = new Mapper(config);
            var members = mapper.Map<IEnumerable<UserDTO>,
                IEnumerable<MemberViewModel>>(await service.Project.GetMembersAsync(model.ProjectId));

            ViewBag.members = members;
            ViewBag.id = model.ProjectId;

            return RedirectToAction("Members", "Project", new { Id = model.ProjectId });
        }

        /// <summary>
        /// Add task page
        /// </summary>
        /// <param name="Id">Id of <see cref="ProjectDTO"/></param>
        /// <returns></returns>
        [HttpGet]
        [IsManagerFilter]
        public async Task<ActionResult> AddTask(int Id)
        {
            await filterManager.IsManager(Id, User.Identity.GetUserId());

            AddTaskViewModel model = new AddTaskViewModel
            {
                GetMembers = await service.Project.GetMembersAsync(Id)
            };
            ViewBag.id = Id;
            return View(model);
        }

        /// <summary>
        /// Add task , httppost method
        /// </summary>
        /// <param name="model"><see cref="AddTaskViewModel"/></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> AddTask(AddTaskViewModel model)
        {
            if (ModelState.IsValid)
            {
                var projectUser = await service.ProjectUser
                    .GetByUserAndProjectAsync(model.UserId, model.ProjectId);

                await service.Task.Add(new TaskDTO
                {
                    Deadline = model.Deadline,
                    Name = model.Name,
                    StatusId = 1,
                    ProjectUserId = projectUser.Id
                });
                ViewBag.id = model.ProjectId;
                return RedirectToAction("Project", "Project", new { Id = model.ProjectId });
            }

            AddTaskViewModel Taskmodel = new AddTaskViewModel
            {
                GetMembers = await service.Project.GetMembersAsync(model.ProjectId)
            };
            ViewBag.id = model.ProjectId;
            return View(Taskmodel);

        }

        /// <summary>
        /// Change task's status
        /// </summary>
        /// <param name="TaskId">Id of <see cref="TaskDTO"/></param>
        /// <param name="StatusId">Id of <see cref="TaskStatusDTO"/></param>
        /// <returns></returns>
        [IsManagerFilter]
        public async Task<ActionResult> SetTaskStatus(int TaskId, int StatusId)
        {

            await filterManager.HasAccess(TaskId, User.Identity.GetUserId());

            var task = await service.Task.GetAsync(TaskId);

            if (task.StatusId == 3)
            {
                var projectUser = await service.ProjectUser.GetAsync(task.ProjectUserId);

                await filterManager.IsManager(projectUser.ProjectId, User.Identity.GetUserId());
            }

            task.StatusId = StatusId;
            await service.Task.Update(task);

            return RedirectToAction("Project", "Project",
                new { Id = service.ProjectUser.Get(task.ProjectUserId).ProjectId });
        }

        /// <summary>
        /// Returns page with user's task on <see cref="ProjectDTO"/>
        /// </summary>
        /// <param name="Id">Id of <see cref="ProjectDTO"/></param>
        /// <returns></returns>
        public async Task<ActionResult> Tasks(int Id)
        {
            ViewBag.id = Id;
            var config = new MapperConfiguration(item
                => item.CreateMap<TaskDTO, TaskViewModel>().ForMember("Status", c
                => c.MapFrom(i => service.TaskStatus.Get(i.StatusId).Name)));
            var mapper = new Mapper(config);

            var tasksDTO = await service.Task.GetUsersTask(User.Identity.GetUserId(), Id);
            var tasks = mapper.Map<IEnumerable<TaskDTO>, IEnumerable<TaskViewModel>>(tasksDTO);

            return View(tasks);
        }

    }
}
