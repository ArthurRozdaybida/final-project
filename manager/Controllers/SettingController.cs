﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using manager.Filters;
using manager.Filters.AdditionalTools;
using manager.ViewModels;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace manager.Controllers
{
    /// <summary>
    /// Controller for project controller
    /// </summary>
    [IsManagerFilter]
    public class SettingController : Controller
    {
        /// <summary>
        /// Give access to services
        /// </summary>
        private IService service;

        /// <summary>
        /// Implement exception logic
        /// </summary>
        private IsProjectLogic filterManager;

        /// <summary>
        /// Constructor , set value to <see cref="service"/>
        /// </summary>
        /// <param name="service"></param>
        public SettingController(IService service)
        {
            this.service = service;
            filterManager = new IsProjectLogic(service);
        }

        /// <summary>
        /// Go to Index page
        /// </summary>
        /// <param name="Id">Id of <see cref="ProjectDTO"/></param>
        /// <returns></returns>
        public async Task<ActionResult> Index(int Id)
        {
            await filterManager.IsManager(Id, User.Identity.GetUserId());

            #region mapping
            //project map
            var configProject = new MapperConfiguration(cfg => cfg.CreateMap<ProjectDTO, ProjectSettingViewModel>()
            .ForMember("Status", c => c.MapFrom(item => service.ProjectStatus.Get(item.StatusId).Name)));
            var mapperProject = new Mapper(configProject);  
            //task map
            var configTask = new MapperConfiguration(cfg => cfg.CreateMap<TaskDTO, TableTaskViewModel>()
            .ForMember("Status", c => c.MapFrom(item => service.TaskStatus.Get(item.StatusId).Name))
            .ForMember("User", c => c.MapFrom(item
             => service.User.Get(service.ProjectUser.Get(item.ProjectUserId).UserId))));
            var mapperTask = new Mapper(configTask);
            //members map 
            var configMember = new MapperConfiguration(cfg
                => cfg.CreateMap<UserDTO, MemberViewModel>()
                .ForMember("ProjectRole", item => item.MapFrom(c
                => service.ProjectUser.GetProjectRole(c.Id, Id).Name)));
            var mapperMember = new Mapper(configMember);
            #endregion

            var project = mapperProject.Map<ProjectDTO, ProjectSettingViewModel>
                (await service.Project.GetAsync(Id));
            project.Tasks = mapperTask.Map<IEnumerable<TaskDTO>,
                IEnumerable<TableTaskViewModel>>(await service.Task.GetProjectTask(Id));
            project.Members = mapperMember.Map<IEnumerable<UserDTO>,
                IEnumerable<MemberViewModel>>(await service.Project.GetMembersAsync(Id));
            ViewBag.id = Id;

            return View(project);
        }

        /// <summary>
        /// Delete task
        /// </summary>
        /// <param name="Id">id of <see cref="Task"/></param>
        /// <returns></returns>
        public async Task<ActionResult> TaskDelete(int Id)
        {
            var task = await service.Task.GetAsync(Id);
            var projectUser = await service.ProjectUser.GetAsync(task.ProjectUserId);
            int projectId = projectUser.ProjectId;

            await filterManager.IsManager(projectId, User.Identity.GetUserId());

            await service.Task.Delete(Id);

            return RedirectToAction("Index", "Setting", new { Id= projectId });
        }

        /// <summary>
        /// Change project's status 
        /// </summary>
        /// <param name="projectId">Id of <see cref="ProjectDTO"/></param>
        /// <param name="statusId">Id of <see cref="ProjectStatusDTO"/></param>
        /// <returns></returns>
        public async Task<ActionResult> ChangeProjectStatus(int projectId, int statusId)
        {
            await filterManager.IsManager(projectId, User.Identity.GetUserId());

            var project = await service.Project.GetAsync(projectId);
            project.StatusId = statusId;

            await service.Project.Update(project);

            ViewBag.id = projectId;

            return RedirectToAction("Index", "Setting", new { Id = projectId });
        }

        /// <summary>
        /// Delele project
        /// </summary>
        /// <param name="Id">Id of <see cref="ProjectDTO"/></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteProject(int Id)
        {
            await filterManager.IsManager(Id, User.Identity.GetUserId());

            await service.Project.Delete(Id);

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Delete project's member 
        /// </summary>
        /// <param name="projId">Id of <see cref="ProjectDTO"/></param>
        /// <param name="userId">Id of <see cref="UserDTO"/></param>
        /// <returns></returns>
        public async Task<ActionResult> DeleteMember(int projId, string userId)
        {
            var projUser = await service.ProjectUser.GetByUserAndProjectAsync(userId, projId);

            var prId = projUser.Id;

            await service.ProjectUser.Delete(prId);

            return RedirectToAction("Index", "Setting", new { Id = projId });
        }

    }
}