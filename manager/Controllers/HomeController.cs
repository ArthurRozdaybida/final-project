﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using manager.ViewModels;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace manager.Controllers
{
    /// <summary>
    /// Home controller
    /// </summary>
    [Authorize]
    public class HomeController : Controller
    {
        /// <summary>
        /// Give access to services
        /// </summary>
        private IService service;

        /// <summary>
        /// Automapper config
        /// </summary>
        private MapperConfiguration config;

        /// <summary>
        /// Constructor, set value to <see cref="service"/>
        /// </summary>
        /// <param name="service"></param>
        public HomeController(IService service)
        {
            this.service = service;

            config = new MapperConfiguration(cfg => cfg.CreateMap<ProjectDTO, ProjectViewModel>()
            .ForMember("StatusName", item
            => item.MapFrom(x => service.ProjectStatus.Get(x.StatusId).Name))
            .ForMember("Percent", item => item.MapFrom(x => (int)(100 * service.Project.GetProgress(x.Id)))));
        }

        /// <summary>
        /// Return all user's projects 
        /// </summary>
        /// <returns></returns>
        public async Task<ActionResult> Index()
        {
            var mapper = new Mapper(config);

            var projectsDTO = await service.User.GetProjectsAsync(User.Identity.GetUserId());
            var projects = mapper.Map<IEnumerable<ProjectDTO>,
                IEnumerable<ProjectViewModel>>(projectsDTO);


            return View(projects);
        }
    }
}