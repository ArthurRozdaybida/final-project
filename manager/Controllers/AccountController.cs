﻿using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using manager.ViewModels;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace manager.Controllers
{
    /// <summary>
    /// Controller for user's tools
    /// </summary>
    public class AccountController : Controller
    {
        /// <summary>
        /// <see cref="IService"/> field
        /// </summary>
        private IService service;

        /// <summary>
        /// Getter for <see cref="IUserService"/>
        /// </summary>
        private IUserService UserService
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<IUserService>();
            }
        }

        /// <summary>
        /// Getter for <see cref="IAuthenticationManager"/>
        /// </summary>
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        /// <summary>
        /// Constructor for creating <see cref="service"/>
        /// </summary>
        /// <param name="service"> <see cref="IService"/></param>
        public AccountController(IService service)
        {
            this.service = service;
        }

        /// <summary>
        /// Login action
        /// </summary>
        /// <returns></returns>
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// Http method for login
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO 
                { 
                    Email = model.Username, 
                    Password = model.Password
                };
                ClaimsIdentity claim = await UserService.Authenticate(userDto);
                if (claim == null)
                {
                    ModelState.AddModelError("", "Неверный логин или пароль.");
                }
                else
                {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(model);
        }

        /// <summary>
        /// Logout method 
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// HttpGet for Registration
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// HttpPost for Registration
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            var users = await UserService.GetAllAsync();
            var email = users.Where(item => item.Email == model.Email);
            var username = users.Where(item => item.UserName == model.UserName);

            if (email.Count() > 0)
                ModelState.AddModelError("Email", "This email has been existed");

            if (username.Count() > 0)
                ModelState.AddModelError("Username", "This username has been existed");

            if (ModelState.IsValid)
            {
                UserDTO userDto = new UserDTO
                {
                    Email = model.Email,
                    Password = model.Password,
                    Name = model.Name,
                    UserName = '@'+model.UserName,
                    Role = "user"
                };
                OperationDetails operationDetails = await UserService.Create(userDto);
                if (operationDetails.Succedeed)
                    return RedirectToAction("Index", "Home");
                else
                    ModelState.AddModelError(operationDetails.Property, operationDetails.Message);
            }
            return View(model);
        }
    }
}