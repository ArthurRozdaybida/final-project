﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace manager.Validators
{
    /// <summary>
    /// Validation logic for specail symbols
    /// </summary>
    public class SpecialSymbolsAttribute : RequiredAttribute
    {
        /// <summary>
        /// Validation use regex
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
            => Regex.IsMatch(value.ToString(), @"^[a-zA-Zа-яА-я0-9 ]*$");

    }
}