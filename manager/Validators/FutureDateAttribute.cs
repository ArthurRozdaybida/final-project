﻿using System;
using System.ComponentModel.DataAnnotations;

namespace manager.Validators
{
    /// <summary>
    /// Future validation
    /// </summary>
    public class FutureDateAttribute : RequiredAttribute
    {
        /// <summary>
        /// Check is choosen date is future 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
            => base.IsValid(value) && ((DateTime)value) > DateTime.Now;
    }
}