﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    /// <summary>
    /// Service for <see cref="ProjectDTO"/>
    /// </summary>
    public class ProjectService : IObjectService<ProjectDTO>
    {
        /// <summary>
        /// Give access to data
        /// </summary>
        private IUnitOfWork Database;

        /// <summary>
        /// Initialize <see cref="Database"/>
        /// </summary>
        /// <param name="uow"></param>
        public ProjectService(IUnitOfWork uow)
        {
            Database = uow;
        }

        /// <summary>
        /// Add <see cref="ProjectDTO"/>
        /// </summary>
        /// <param name="item"><see cref="ProjectDTO"/></param>
        /// <returns></returns>
        public async Task Add(ProjectDTO item)
        {
            Database.Project.Add(new Project()
            {
                Link = item.Link,
                Name = item.Name,
                StatusId = item.StatusId
            });

            await Database.SaveAsync();
        }

        /// <summary>
        /// Delete <see cref="ProjectDTO"/>
        /// </summary>
        /// <param name="id">Id of <see cref="ProjectDTO"/></param>
        /// <returns></returns>
        public async Task Delete(int id)
        {
            Database.Project.Delete(id);

            await Database.SaveAsync();
        }

        /// <summary>
        /// Get all <see cref="ProjectDTO"/> async method
        /// </summary>
        /// <returns> <see cref="IEnumerable{ProjectDTO}"/></returns>
        public async Task<IEnumerable<ProjectDTO>> GetAllAsync()
        {
            var mapper = new MapperConfiguration(cfg 
                => cfg.CreateMap<Project, ProjectDTO>()).CreateMapper();

            return mapper.Map<IEnumerable<Project>, 
                IEnumerable<ProjectDTO>>(await Database.Project.GetAllAsync());
        }

        /// <summary>
        /// Get <see cref="ProjectDTO"/> by id, async method
        /// </summary>
        /// <param name="id"> Id of <see cref="ProjectDTO"/></param>
        /// <returns> <see cref="ProjectDTO"/></returns>
        public async Task<ProjectDTO> GetAsync(int id)
        {
            var mapper = new MapperConfiguration(cfg
                => cfg.CreateMap<Project, ProjectDTO>()).CreateMapper();

            return mapper.Map<Project, ProjectDTO>(await Database.Project.GetAsync(id));
        }

        /// <summary>
        /// Update <see cref="ProjectDTO"/>
        /// </summary>
        /// <param name="item"><see cref="ProjectDTO"/></param>
        /// <returns></returns>
        public async Task Update(ProjectDTO item)
        {
            var mapper = new MapperConfiguration(cfg 
                => cfg.CreateMap<ProjectDTO, Project>()).CreateMapper();

            var project = mapper.Map<ProjectDTO, Project>(item);
            Database.Project.Update(project);

            await Database.SaveAsync();
        }

        /// <summary>
        /// Return users who not a part of this project
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<UserDTO>> GetMembersAsync(int projectId)
        {
            IEnumerable<ProjectUser> projectUsers = await Database.ProjectUser.GetAllAsync();

            var projectUsers1 = projectUsers.Where(item => item.ProjectId == projectId);

            var members = projectUsers1.Select(item => Database.UserManager.Get(item.UserId));

            var mapper = new MapperConfiguration(cfg
                => cfg.CreateMap<ApplicationUser, UserDTO>()).CreateMapper();

            return mapper.Map< IEnumerable<ApplicationUser>, IEnumerable<UserDTO>>(members);
        }

        /// <summary>
        /// Get <see cref="ProjectDTO"/> progress
        /// </summary>
        /// <param name="Id">Id of <see cref="ProjectDTO"/></param>
        /// <returns> Progress of <see cref="ProjectDTO"/></returns>
        public double GetProgress(int Id)
        {
            var projectUsers1 = Database.ProjectUser.GetAll();

            var projectUsers = projectUsers1  
                .Where(item => item.ProjectId == Id)
                .Select(item => item.Id).ToList();

            var All = Database.Task.GetAll().Where(item => projectUsers
            .Contains(item.ProjectUserId));

            int AllNum = All.Count();
            if (AllNum == 0)
                return 0;

            var Completed = All.Where(item => item.StatusId==4).Count();

            return (double)Completed / AllNum;
        }
    }
}
