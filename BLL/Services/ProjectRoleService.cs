﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    /// <summary>
    /// Srvoce for <see cref="ProjectRoleDTO"/>
    /// </summary>
    public class ProjectRoleService : IRoleStatusService<ProjectRoleDTO>
    {
        /// <summary>
        /// Give access to data
        /// </summary>
        private IUnitOfWork Database;

        /// <summary>
        /// Initialize <see cref="Database"/>
        /// </summary>
        /// <param name="db"> <see cref="IUnitOfWork"/></param>
        public ProjectRoleService(IUnitOfWork iow)
        {
            Database = iow;
        }

        /// <summary>
        /// Get all <see cref="ProjectRoleDTO"/> async method
        /// </summary>
        /// <returns> <see cref="IEnumerable{ProjectRoleDTO}"/></returns>
        public async Task<IEnumerable<ProjectRoleDTO>> GetAllAsync()
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProjectRole, ProjectRoleDTO>())
                .CreateMapper();

            return mapper.Map<IEnumerable<ProjectRole>, 
                IEnumerable<ProjectRoleDTO>>(await Database.ProjectRole.GetAllAsync());
        }

        /// <summary>
        /// Get item by id <see cref="ProjectRoleDTO"/> async method
        /// </summary>
        /// <param name="id"> Id of <see cref="ProjectRoleDTO"/></param>
        /// <returns> <see cref="ProjectRoleDTO"/></returns>
        public async Task<ProjectRoleDTO> GetAsync(int id)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ProjectRole, ProjectRoleDTO>())
               .CreateMapper();

            return mapper.Map<ProjectRole, ProjectRoleDTO>(await Database.ProjectRole.GetAsync(id));
        }
    }
}
