﻿using DAL.Interfaces;
using BLL.DTO;
using BLL.Interfaces;
using System.Threading.Tasks;
using System.Collections.Generic;
using AutoMapper;
using DAL.Entities;

namespace BLL.Services
{
    /// <summary>
    /// Service for <see cref="ProjectStatusDTO"/>
    /// </summary>
    public class ProjectStatusService : IRoleStatusService<ProjectStatusDTO>
    {
        /// <summary>
        /// Give access to data
        /// </summary>
        private IUnitOfWork Database;

        /// <summary>
        /// Initialize <see cref="Database"/>
        /// </summary>
        /// <param name="iow"></param>
        public ProjectStatusService(IUnitOfWork iow)
        {
            Database = iow;
        }

        /// <summary>
        /// Get all <see cref="ProjectStatusDTO"/> async method
        /// </summary>
        /// <returns> <see cref="IEnumerable{ProjectStatusDTO}"/></returns>
        public async Task<IEnumerable<ProjectStatusDTO>> GetAllAsync()
        {
            var mapper = new MapperConfiguration(cfg => cfg
            .CreateMap<ProjectStatus, ProjectStatusDTO>()).CreateMapper();

            return mapper.Map<IEnumerable<ProjectStatus>, 
                IEnumerable<ProjectStatusDTO>>(await Database.ProjectStatus.GetAllAsync());
        }

        /// <summary>
        /// Get <see cref="ProjectStatusDTO"/> by id
        /// </summary>
        /// <param name="id"> Id of <see cref="ProjectStatusDTO"/></param>
        /// <returns> <see cref="ProjectStatusDTO"/></returns>
        public async Task<ProjectStatusDTO> GetAsync(int id)
        {
            var mapper = new MapperConfiguration(cfg => cfg
            .CreateMap<ProjectStatus, ProjectStatusDTO>()).CreateMapper();

            return mapper.Map<ProjectStatus, ProjectStatusDTO>( await Database.ProjectStatus.GetAsync(id));
        }

        /// <summary>
        /// Get <see cref="ProjectStatusDTO"/> by id
        /// </summary>
        /// <param name="id"> Id of <see cref="ProjectStatusDTO"/></param>
        /// <returns> <see cref="ProjectStatusDTO"/></returns>
        public ProjectStatusDTO Get(int id)
        {
            var mapper = new MapperConfiguration(cfg => cfg
            .CreateMap<ProjectStatus, ProjectStatusDTO>()).CreateMapper();

            return mapper.Map<ProjectStatus, ProjectStatusDTO>(Database.ProjectStatus.Get(id));
        }
    }
}
