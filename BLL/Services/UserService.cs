﻿using AutoMapper;
using BLL.DTO;
using BLL.Infrastructure;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BLL.Services
{
    /// <summary>
    /// Services for User , implement authentication and crud methods  
    /// </summary>
    public class UserService : IUserService
    {
        /// <summary>
        /// <see cref="IUnitOfWork"/> implement access to data
        /// </summary>
        IUnitOfWork Database { get; set; }

        /// <summary>
        /// Constructor 
        /// </summary>
        /// <param name="uow"> <see cref="IUnitOfWork"/> </param>
        public UserService(IUnitOfWork uow)
        {
            Database = uow;
        }

        /// <summary>
        /// Register new <see cref="UserDTO"/>
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        public async Task<OperationDetails> Create(UserDTO userDto)
        {
            ApplicationUser user = await Database.UserManager.FindByEmailAsync(userDto.Email);
            if (user == null)
            {
                user = new ApplicationUser 
                { 
                    Email = userDto.Email, 
                    UserName = userDto.UserName,
                    Name = userDto.Name
                };
                var result = await Database.UserManager.CreateAsync(user, userDto.Password);
                if (result.Errors.Count() > 0)
                    return new OperationDetails(false, result.Errors.FirstOrDefault(), "");

                await Database.UserManager.AddToRoleAsync(user.Id, userDto.Role);

                await Database.SaveAsync();
                return new OperationDetails(true, "Registration done", "");
            }
            else
            {
                return new OperationDetails(false, "Login has already been exist", "Email");
            }
        }

        /// <summary>
        /// Authentication method
        /// </summary>
        /// <param name="userDto"><see cref="UserDTO"/></param>
        /// <returns><see cref="ClaimsIdentity"/></returns>
        public async Task<ClaimsIdentity> Authenticate(UserDTO userDto)
        {
            ClaimsIdentity claim = null;

            ApplicationUser user = await Database.UserManager.FindAsync(userDto.Email, userDto.Password);
            if (user != null)
                claim = await Database.UserManager.CreateIdentityAsync(user,
                                            DefaultAuthenticationTypes.ApplicationCookie);
            return claim;
        }

        /// <summary>
        /// Dispose method
        /// </summary>
        public void Dispose()
        {
            Database.Dispose();
        }

        /// <summary>
        /// Get <see cref="UserDTO"/> by id
        /// </summary>
        /// <param name="id">Id of <see cref="UserDTO"/></param>
        /// <returns><see cref="UserDTO"/></returns>
        public UserDTO Get(string id)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ApplicationUser, UserDTO>()).CreateMapper();
            
            return mapper.Map<ApplicationUser, UserDTO>(Database.UserManager.Get(id));
        }

        /// <summary>
        /// Get <see cref="UserDTO"/> async method
        /// </summary>
        /// <param name="id">Id of <see cref="UserDTO"/></param>
        /// <returns><see cref="UserDTO"/></returns>
        public async Task<UserDTO> GetAsync(string id)
        {
            var mapper = new MapperConfiguration(cfg => cfg.CreateMap<ApplicationUser, UserDTO>()).CreateMapper();

            return  mapper.Map<ApplicationUser, UserDTO>(await Database.UserManager.GetAsync(id));
        }

        /// <summary>
        /// Get all <see cref="UserDTO"/>
        /// </summary>
        /// <returns><see cref="IEnumerable{UserDTO}"/></returns>
        public IEnumerable<UserDTO> GetAll()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ApplicationUser, UserDTO>()
            .ForMember("Role", c => c.MapFrom(item => Database.UserManager.GetRoles(item.Id).FirstOrDefault())));

            var mapper = new Mapper(config);
            return mapper.Map<IEnumerable<ApplicationUser>, IEnumerable<UserDTO>>(Database.UserManager.GetAll());
        }

        /// <summary>
        /// Get all <see cref="UserDTO"/> async meethod
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<ApplicationUser, UserDTO>()
            .ForMember("Role", c => c.MapFrom(item => Database.UserManager.GetRoles(item.Id).FirstOrDefault())));
            var mapper = new Mapper(config);

            return mapper.Map<IEnumerable<ApplicationUser>, IEnumerable<UserDTO>>(await Database.UserManager.GetAllAsync());
        }

        /// <summary>
        /// Delete async method
        /// </summary>
        /// <param name="id">Id of <see cref="UserDTO"/></param>
        /// <returns></returns>
        public async Task DeleteAsync(string id)
        {
            await Database.UserManager.DeleteAsync(id);
            await Database.SaveAsync();
        }

        /// <summary>
        /// Delete <see cref="UserDTO"/>
        /// </summary>
        /// <param name="id">Id of <see cref="UserDTO"/></param>
        public void Delete(string id)
        {
            Database.UserManager.Delete(id);
            Database.Save();
        }

        /// <summary>
        /// Get user's prjects
        /// </summary>
        /// <param name="UserId">Id of <see cref="UserDTO"/></param>
        /// <returns> <see cref="IEnumerable{ProjectDTO}"/></returns>
        public async Task<IEnumerable<ProjectDTO>> GetProjectsAsync(string UserId)
        {
            IEnumerable<ProjectUser> projectUsers = await Database.ProjectUser.GetAllAsync();
            projectUsers = projectUsers.Where(item => item.UserId == UserId);

            var projects =  projectUsers.Select(item => Database.Project.Get(item.ProjectId));

            var mapper = new MapperConfiguration(cfg 
                => cfg.CreateMap<Project, ProjectDTO>()).CreateMapper();

            return mapper.Map<IEnumerable<Project>, IEnumerable<ProjectDTO>>(projects);
        }
         
        /// <summary>
        /// Get free members
        /// </summary>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<UserDTO>> GetFreeMembers(int projectId)
        {
            IEnumerable<ProjectUser> projectUsers = await Database.ProjectUser.GetAllAsync();
            IEnumerable<ApplicationUser> users = await Database.UserManager.GetAllAsync();
            
            var members = projectUsers
                .Where(item => item.ProjectId == projectId)
                .Select(item => item.UserId);

            var freeUser = users.Where(item => !members.Contains(item.Id));

            var mapper = new MapperConfiguration(cfg
                => cfg.CreateMap<ApplicationUser, UserDTO>()).CreateMapper();
            return mapper.Map<IEnumerable<ApplicationUser>, IEnumerable<UserDTO>>(freeUser);
        }
        /// <summary>
        /// Change user role
        /// </summary>
        /// <param name="id">Id of <see cref="UserDTO"/></param>
        /// <param name="Name">Name of <see cref="ApplicationRole"/></param>
        /// <returns></returns>
        public async Task ChangeUserRole(string id, string Name)
        {
            var user = await  Database.UserManager.GetAsync(id);
            var role = await Database.RoleManager
                .FindByIdAsync(user.Roles.FirstOrDefault().RoleId);

            Database.UserManager.RemoveFromRole(user.Id, role.Name);

            Database.UserManager.AddToRole(user.Id, Name);
            
            await Database.UserManager.UpdateAsync(user);
            await Database.SaveAsync();
        }
    }
}
