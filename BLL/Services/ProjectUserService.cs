﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    /// <summary>
    /// Service for <see cref="ProjectUserDTO"/>
    /// </summary>
    public class ProjectUserService: IObjectService<ProjectUserDTO>
    {
        /// <summary>
        /// Give access to data
        /// </summary>
        private IUnitOfWork Database;

        /// <summary>
        /// Initialize  
        /// </summary>
        /// <param name="uow"></param>
        public ProjectUserService(IUnitOfWork uow)
        {
            Database = uow;
        }

        /// <summary>
        /// Add <see cref="ProjectUserDTO"/>
        /// </summary>
        /// <param name="item"> <see cref="ProjectUserDTO"/></param>
        /// <returns></returns>
        public async Task Add(ProjectUserDTO item)
        {
            Database.ProjectUser.Add(new ProjectUser()
            {
                ProjectId = item.ProjectId,
                RoleId = item.RoleId,
                UserId = item.UserId,
            });

            await Database.SaveAsync();
        }

        /// <summary>
        /// Delete <see cref="ProjectUserDTO"/>
        /// </summary>
        /// <param name="id">Id of <see cref="ProjectUserDTO"/></param>
        /// <returns></returns>
        public async Task Delete(int id)
        {
            Database.ProjectUser.Delete(id);

            await Database.SaveAsync();
        }

        /// <summary>
        /// Get all <see cref="ProjectUserDTO"/> async method
        /// </summary>
        /// <returns> <see cref="IEnumerable{ProjectUserDTO}"/></returns>
        public async Task<IEnumerable<ProjectUserDTO>> GetAllAsync()
        {
            var mapper = new MapperConfiguration(cfg 
                => cfg.CreateMap<ProjectUser, ProjectUserDTO>()).CreateMapper();

            return mapper.Map<IEnumerable<ProjectUser>, 
                IEnumerable<ProjectUserDTO>>(await Database.ProjectUser.GetAllAsync());
        }

        /// <summary>
        /// Get <see cref="ProjectDTO"/> by id, async method
        /// </summary>
        /// <param name="id">Id of <see cref="ProjectUserDTO"/></param>
        /// <returns> <see cref="ProjectUserDTO"/></returns>
        public async Task<ProjectUserDTO> GetAsync(int id)
        {
            var mapper = new MapperConfiguration(cfg
                => cfg.CreateMap<ProjectUser, ProjectUserDTO>()).CreateMapper();

            return mapper.Map<ProjectUser, 
                ProjectUserDTO>(await Database.ProjectUser.GetAsync(id));
        }

        /// <summary>
        /// Get <see cref="ProjectDTO"/> by id
        /// </summary>
        /// <param name="id">Id of <see cref="ProjectUserDTO"/></param>
        /// <returns> <see cref="ProjectUserDTO"/></returns>
        public ProjectUserDTO Get(int id)
        {
            var mapper = new MapperConfiguration(cfg
                => cfg.CreateMap<ProjectUser, ProjectUserDTO>()).CreateMapper();

            return mapper.Map<ProjectUser,
                ProjectUserDTO>(Database.ProjectUser.Get(id));
        }

        /// <summary>
        /// Update <see cref="ProjectUserDTO"/>
        /// </summary>
        /// <param name="item"> <see cref="ProjectUserDTO"/></param>
        /// <returns></returns>
        public async Task Update(ProjectUserDTO item)
        {
            var mapper = new MapperConfiguration(cfg
                => cfg.CreateMap<ProjectUserDTO, ProjectUser>()).CreateMapper();

            Database.ProjectUser.Update(mapper.Map<ProjectUserDTO, ProjectUser>(item));
            await Database.SaveAsync();
        }


        /// <summary>
        /// Get user's role on project
        /// </summary>
        /// <param name="user">Id of <see cref="UserDTO"/></param>
        /// <param name="project">Id of <see cref="ProjectDTO"/></param>
        /// <returns></returns>
        public ProjectRoleDTO GetProjectRole(string user, int project)
        {
            var roles = Database.ProjectUser.GetAll();
            var roleId = roles.Where(item 
                => item.ProjectId == project && item.UserId == user).FirstOrDefault();

            var role = Database.ProjectRole.Get(roleId.RoleId);
            var mapper = new MapperConfiguration(cfg 
                => cfg.CreateMap<ProjectRole, ProjectRoleDTO>()).CreateMapper();
            return mapper.Map<ProjectRole, ProjectRoleDTO>(role);
        }

        /// <summary>
        /// Get projectuser instance by project's and user's ids
        /// </summary>
        /// <param name="User">Id of <see cref="UserDTO"/></param>
        /// <param name="Project">id of <see cref="ProjectDTO"/></param>
        /// <returns><see cref="ProjectUserDTO"/> instance </returns>
        public async Task<ProjectUserDTO> GetByUserAndProjectAsync(string User, int Project)
        {
            var projectUsers = await Database.ProjectUser.GetAllAsync();

            var result = projectUsers.Where(item
                => item.ProjectId == Project && item.UserId == User).FirstOrDefault();

            var mapper = new MapperConfiguration(cfg
                => cfg.CreateMap<ProjectUser, ProjectUserDTO>()).CreateMapper();
            return mapper.Map<ProjectUser, ProjectUserDTO>(result);
        }

    }
}
