﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.Services
{
    /// <summary>
    /// Service for <see cref="TaskDTO"/>
    /// </summary>
    public class TaskService : IObjectService<TaskDTO>
    {
        /// <summary>
        /// Give access to data
        /// </summary>
        private IUnitOfWork Database;

        /// <summary>
        /// Initialize <see cref="Database"/>
        /// </summary>
        /// <param name="uow"></param>
        public TaskService(IUnitOfWork uow)
        {
            Database = uow;
        }

        /// <summary>
        /// Add <see cref="TaskDTO"/>
        /// </summary>
        /// <param name="item"> <see cref="TaskDTO"/></param>
        /// <returns></returns>
        public async Task Add(TaskDTO item)
        {
            Database.Task.Add(new ProjectTask()
            {
                Name = item.Name,
                Deadline = item.Deadline,
                StatusId = item.StatusId,
                ProjectUserId = item.ProjectUserId
            });

            await Database.SaveAsync();
        }

        /// <summary>
        /// Delete <see cref="TaskDTO"/>
        /// </summary>
        /// <param name="id">Id of <see cref="TaskDTO"/></param>
        /// <returns></returns>
        public async Task Delete(int id)
        {
            Database.Task.Delete(id);

            await Database.SaveAsync();
        }

        /// <summary>
        /// Get all <see cref="TaskDTO"/> async method
        /// </summary>
        /// <returns> <see cref="IEnumerable{TaskDTO}"/></returns>
        public async Task<IEnumerable<TaskDTO>> GetAllAsync()
        {
            var mapper = new MapperConfiguration(cfg 
                => cfg.CreateMap<ProjectTask, TaskDTO>()).CreateMapper();

            return mapper.Map<IEnumerable<ProjectTask>, 
                IEnumerable<TaskDTO>>(await Database.Task.GetAllAsync());
        }

        /// <summary>
        /// Get <see cref="TaskDTO"/> by id
        /// </summary>
        /// <param name="id">Id of <see cref="TaskDTO"/></param>
        /// <returns><see cref="TaskDTO"/></returns>
        public async Task<TaskDTO> GetAsync(int id)
        {
            var mapper = new MapperConfiguration(cfg
                => cfg.CreateMap<ProjectTask, TaskDTO>()).CreateMapper();

            return mapper.Map<ProjectTask,
                TaskDTO>(await Database.Task.GetAsync(id));
        }

        /// <summary>
        /// Update <see cref="TaskDTO"/>
        /// </summary>
        /// <param name="item"><see cref="TaskDTO"/></param>
        /// <returns></returns>
        public async Task Update(TaskDTO item)
        {
            var mapper = new MapperConfiguration(cfg 
                => cfg.CreateMap<TaskDTO, ProjectTask>()).CreateMapper();

            var task = mapper.Map<TaskDTO, ProjectTask>(item);

            Database.Task.Update(task);

            await Database.SaveAsync();
        }

        /// <summary>
        /// Return's users tasks
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="projectId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<TaskDTO>>GetUsersTask(string userId, int projectId)
        {
            var projectUsers = await Database.ProjectUser.GetAllAsync();

            int projectUserId = projectUsers.Where(item 
                => item.UserId == userId 
                && item.ProjectId == projectId).FirstOrDefault().Id;

            var tasks = await Database.Task.GetAllAsync();

            var config = new MapperConfiguration(cfg 
                => cfg.CreateMap<ProjectTask, TaskDTO>()).CreateMapper();
            return config.Map<IEnumerable<ProjectTask>, IEnumerable<TaskDTO>>
                (tasks.Where(item => item.ProjectUserId == projectUserId));
        }

        /// <summary>
        /// Return <see cref="TaskDTO"/>with ProjectUser instance
        /// </summary>
        /// <returns><see cref="IEnumerable{TaskDTO}"/></returns>
        public async Task<IEnumerable<TaskDTO>> GetProjectTask(int Id)
        {
            var projectUsers1 = await Database.ProjectUser.GetAllAsync();

            var projectUsers = projectUsers1
                .Where(item => item.ProjectId == Id)
                .Select(item=>item.Id).ToList();

            var allTasks = await Database.Task.GetAllAsync();

            var config = new MapperConfiguration(cfg
                => cfg.CreateMap<ProjectTask, TaskDTO>()).CreateMapper();
            return config.Map<IEnumerable<ProjectTask>, IEnumerable<TaskDTO>>(allTasks
                .Where(item => projectUsers.Contains(item.ProjectUserId)));
        }
    }
}
