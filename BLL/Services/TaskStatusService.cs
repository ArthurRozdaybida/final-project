﻿using AutoMapper;
using BLL.DTO;
using BLL.Interfaces;
using DAL.Entities;
using DAL.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    /// <summary>
    /// Service for <see cref="TaskStatusDTO"/>
    /// </summary>
    public class TaskStatusService : IRoleStatusService<TaskStatusDTO>
    {
        /// <summary>
        /// Give access to data
        /// </summary>
        private IUnitOfWork Database;

        /// <summary>
        /// Initialize <see cref="Database"/>
        /// </summary>
        /// <param name="iow"></param>
        public TaskStatusService(IUnitOfWork iow)
        {
            Database = iow;
        }

        /// <summary>
        /// Get all <see cref="TaskStatusDTO"/> async method
        /// </summary>
        /// <returns> <see cref="IEnumerable{TaskStatusDTO}"/></returns>
        public async Task<IEnumerable<TaskStatusDTO>> GetAllAsync()
        {
            var mapper = new MapperConfiguration(cfg => 
            cfg.CreateMap<ProjectTaskStatus, TaskStatusDTO>()).CreateMapper();

            return mapper.Map<IEnumerable<ProjectTaskStatus>, 
                IEnumerable<TaskStatusDTO>>(await Database.TaskStatus.GetAllAsync());
        }

        /// <summary>
        /// Get <see cref="TaskStatusDTO"/> by id, async method 
        /// </summary>
        /// <param name="id"> Id of <see cref="TaskStatusDTO"/></param>
        /// <returns> <see cref="TaskStatusDTO"/></returns>
        public async Task<TaskStatusDTO> GetAsync(int id)
        {
            var mapper = new MapperConfiguration(cfg =>
            cfg.CreateMap<ProjectTaskStatus, TaskStatusDTO>()).CreateMapper();

            return mapper.Map<ProjectTaskStatus, TaskStatusDTO>(await Database.TaskStatus.GetAsync(id));
        }

        /// <summary>
        /// Get name of status for ViewModels
        /// </summary>
        /// <param name="id">Id of <see cref="TaskStatusDTO"/></param>
        /// <returns>Name if <see cref="TaskStatusDTO"/></returns>
        public TaskStatusDTO Get(int id)
        {
            var mapper = new MapperConfiguration(cfg =>
            cfg.CreateMap<ProjectTaskStatus, TaskStatusDTO>()).CreateMapper();

            return mapper.Map<ProjectTaskStatus, 
                TaskStatusDTO>(Database.TaskStatus.Get(id));
        }
    }
}
