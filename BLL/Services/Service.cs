﻿using BLL.Interfaces;
using DAL.Identity;
using DAL.Interfaces;

namespace BLL.Services
{
    /// <summary>
    /// Service , contain all another services
    /// </summary>
    public class Service : IService
    {
        /// <summary>
        /// Give access to database
        /// </summary>
        private IUnitOfWork Database;

        #region services
        private UserService userService;
        private ApplicationUserManager userManager;
        private ApplicationRoleManager roleManager;

        /// <summary>
        /// Service for project's roles
        /// </summary>
        private ProjectRoleService projectRoleService;

        /// <summary>
        /// Service for project
        /// </summary>
        private ProjectService projectService;

        /// <summary>
        /// Service for project's statuses
        /// </summary>
        private ProjectStatusService projectStatusService;

        /// <summary>
        /// Service for projectUser
        /// </summary>
        private ProjectUserService projectUserService;

        /// <summary>
        /// Service for task
        /// </summary>
        private TaskService taskService;

        /// <summary>
        /// Task's statuses service
        /// </summary>
        private TaskStatusService taskStatusService;
        #endregion


        /// <summary>
        /// Constructor which initialize <see cref="Database"/>
        /// </summary>
        /// <param name="db"></param>
        public Service(IUnitOfWork db)
        {
            Database = db;
        }

        /// <summary>
        /// Property for <see cref="userManager"/>
        /// </summary>
        public UserService User
        {
            get
            {
                if (userService is null)
                    userService = new UserService(Database);
                return userService;
            }
        }

        /// <summary>
        /// Property for <see cref="projectRoleService"/>
        /// </summary>
        public ProjectRoleService ProjectRole
        {
            get
            {
                if (projectRoleService is null)
                    projectRoleService = new ProjectRoleService(Database);
                return projectRoleService;
            }
        }

        /// <summary>
        /// Property for <see cref="projectService"/>
        /// </summary>
        public ProjectService Project
        {
            get
            {
                if (projectService is null)
                    projectService = new ProjectService(Database);
                return projectService;
            }
        }

        /// <summary>
        /// Property for <see cref="projectStatusService"/>
        /// </summary>
        public ProjectStatusService ProjectStatus
        {
            get
            {
                if (projectStatusService is null)
                    projectStatusService = new ProjectStatusService(Database);
                return projectStatusService;
            }
        }

        /// <summary>
        /// Property for <see cref="projectUserService"/>
        /// </summary>
        public ProjectUserService ProjectUser
        {
            get
            {
                if (projectUserService is null)
                    projectUserService = new ProjectUserService(Database);
                return projectUserService;
            }
        }

        /// <summary>
        /// Property for <see cref="taskService"/>
        /// </summary>
        public TaskService Task
        {
            get
            {
                if (taskService is null)
                    taskService = new TaskService(Database);
                return taskService;
            }
        }

        /// <summary>
        /// Property for <see cref="taskStatusService"/>
        /// </summary>
        public TaskStatusService TaskStatus
        {
            get
            {
                if (taskStatusService is null)
                    taskStatusService = new TaskStatusService(Database);
                return taskStatusService;
            }
        }
    }
}
