﻿using DAL.Entities;

namespace BLL.DTO
{
    /// <summary>
    /// DTO model for <see cref="ProjectUser"/>
    /// </summary>
    public class ProjectUserDTO
    {
        /// <summary>
        /// Id of <see cref="ProjectUser"/>
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Id of <see cref="Project"/>
        /// </summary>
        public int ProjectId { get; set; }

        /// <summary>
        /// Id of <see cref="ApplicationUser"/>
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Id of <see cref="ProjectRole"/>
        /// </summary>
        public int RoleId { get; set; }
    }
}
