﻿using DAL.Entities;

namespace BLL.DTO
{
    /// <summary>
    /// DTO model for <see cref="ProjectTaskStatus"/>
    /// </summary>
    public class TaskStatusDTO
    {
        /// <summary>
        /// Id of <see cref="ProjectTaskStatus"/>
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of <see cref="ProjectTaskStatus"/>
        /// </summary>
        public string Name { get; set; }
    }
}
