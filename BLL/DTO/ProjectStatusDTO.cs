﻿using DAL.Entities;

namespace BLL.DTO
{
    /// <summary>
    /// DTO model for project status
    /// </summary>
    public class ProjectStatusDTO
    {
        /// <summary>
        /// Id of <see cref="ProjectStatus"/>
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Status's name
        /// </summary>
        public string Name { get; set; }
    }
}
