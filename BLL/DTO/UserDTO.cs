﻿using DAL.Entities;

namespace BLL.DTO
{
    /// <summary>
    /// DTO model for <see cref="ApplicationUser"/>
    /// </summary>
    public class UserDTO
    {
        /// <summary>
        /// Id of <see cref="ApplicationUser"/>
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Email of <see cref="ApplicationUser"/>
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Password of <see cref="ApplicationUser"/>
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Username of <see cref="ApplicationUser"/>
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Name of <see cref="ApplicationUser"/>
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// User's role <see cref="ApplicationRole"/>
        /// </summary>
        public string Role { get; set; }
    }
}
