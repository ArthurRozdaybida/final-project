﻿using DAL.Entities;

namespace BLL.DTO
{
    /// <summary>
    /// DTO model for <see cref="DAL.Entities.ProjectRole"/>
    /// </summary>
    public class ProjectRoleDTO
    {
        /// <summary>
        /// Id of <see cref="ProjectRole"/>
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of role
        /// </summary>
        public string Name { get; set; }
    }
}
