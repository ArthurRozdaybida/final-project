﻿using System.ComponentModel.DataAnnotations;
using DAL.Entities;

namespace BLL.DTO
{
    /// <summary>
    /// DTO model for <see cref="Project"/>
    /// </summary>
    public class ProjectDTO
    {
        /// <summary>
        /// Id of <see cref="ProjectDTO"/>
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of project 
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Id of <see cref="ProjectStatusDTO"/>
        /// </summary>
        [Required]
        public int StatusId { get; set; }

        /// <summary>
        /// Link to repository or some another sources
        /// </summary>
        [MinLength(7)]
        public string Link { get; set; }
    }
}
