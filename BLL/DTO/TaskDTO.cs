﻿using DAL.Entities;
using System;

namespace BLL.DTO
{
    /// <summary>
    /// DTO model for <see cref="ProjectTask"/>
    /// </summary>
    public class TaskDTO
    {
        /// <summary>
        /// Id of <see cref="ProjectTask"/>
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of <see cref="ProjectTask"/>
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Deadline of <see cref="ProjectTask"/>
        /// </summary>
        public DateTime Deadline { get; set; }

        /// <summary>
        /// Id of <see cref="ProjectTaskStatus"/>
        /// </summary>
        public int StatusId { get; set; }

        /// <summary>
        /// Id of <see cref="ProjectUser"/>
        /// </summary>
        public int ProjectUserId { get; set; }

    }
}
