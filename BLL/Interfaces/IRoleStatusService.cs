﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    /// <summary>
    /// Interface for roles and statuses
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IRoleStatusService <T> where T : class
    {
        /// <summary>
        /// Get <see cref="T"/> async method
        /// </summary>
        /// <param name="id"> Id of <see cref="T"/></param>
        /// <returns> <see cref="T"/></returns>
        Task<T> GetAsync(int id);

        /// <summary>
        /// Get alll <see cref="T"/> async method
        /// </summary>
        /// <returns> <see cref="IEnumerable{T}"/></returns>
        Task<IEnumerable<T>> GetAllAsync();
    }
}
