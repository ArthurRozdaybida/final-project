﻿using BLL.DTO;
using BLL.Infrastructure;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    /// <summary>
    /// Set methods for User/>
    /// </summary>
    public interface IUserService : IDisposable
    {
        /// <summary>
        /// Create new User
        /// </summary>
        /// <param name="userDto"><see cref="UserDTO"/></param>
        /// <returns></returns>
        Task<OperationDetails> Create(UserDTO userDto);

        /// <summary>
        /// Authenticate user
        /// </summary>
        /// <param name="userDto"><see cref="UserDTO"/></param>
        /// <returns><see cref="ClaimsIdentity"/></returns>
        Task<ClaimsIdentity> Authenticate(UserDTO userDto);

        /// <summary>
        /// Get <see cref="UserDTO"/> async 
        /// </summary>
        /// <param name="id">Id of <see cref="UserDTO"/></param>
        /// <returns><see cref="UserDTO"/></returns>
        Task<UserDTO> GetAsync(string id);

        /// <summary>
        /// Get all <see cref="UserDTO"/>
        /// </summary>
        /// <returns><see cref="IEnumerable{UserDTO}"/></returns>
        Task<IEnumerable<UserDTO>> GetAllAsync();

        /// <summary>
        /// Get <see cref="UserDTO"/> by id
        /// </summary>
        /// <param name="id">Id of <see cref="UserDTO"/></param>
        /// <returns><see cref="UserDTO"/></returns>
        UserDTO Get(string id);

        /// <summary>
        /// Get all <see cref="UserDTO"/>
        /// </summary>
        /// <returns> <see cref="IEnumerable{UserDTO}"/></returns>
        IEnumerable<UserDTO> GetAll();

        /// <summary>
        /// Delete <see cref="UserDTO"/>
        /// </summary>
        /// <param name="id">Id <see cref="UserDTO"/></param>
        /// <returns></returns>
        Task DeleteAsync(string id);

        /// <summary>
        /// Delete <see cref="UserDTO"/>
        /// </summary>
        /// <param name="id">Id of <see cref="UserDTO"/></param>
        void Delete(string id);
    }
}
