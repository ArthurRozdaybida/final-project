﻿using BLL.Services;

namespace BLL.Interfaces
{
    /// <summary>
    /// Inteface for service(use IOW pattern)
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// User service
        /// </summary>
        UserService User { get; }
        
        /// <summary>
        /// Project role service 
        /// </summary>
        ProjectRoleService ProjectRole { get; }

        /// <summary>
        /// Project service
        /// </summary>
        ProjectService Project { get; }

        /// <summary>
        /// Project's status service
        /// </summary>
        ProjectStatusService ProjectStatus { get; }

        /// <summary>
        /// ProjectUser service
        /// </summary>
        ProjectUserService ProjectUser { get; }

        /// <summary>
        /// Task service
        /// </summary>
        TaskService Task { get; }

        /// <summary>
        /// Task's status service
        /// </summary>
        TaskStatusService TaskStatus{ get; }
    }
}
