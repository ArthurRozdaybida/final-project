﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    /// <summary>
    /// Default set of actions for service
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IObjectService <T> where T : class
    {
        /// <summary>
        /// Get <see cref="T"/>
        /// </summary>
        /// <param name="id">id of <see cref="T"/></param>
        /// <returns><see cref="T"/></returns>
        Task<T> GetAsync(int id);

        /// <summary>
        /// Get all <see cref="T"/>
        /// </summary>
        /// <returns><see cref="IEnumerable{T}"/></returns>
        Task<IEnumerable<T>> GetAllAsync();

        /// <summary>
        /// Add <see cref="T"/> to DbSet
        /// </summary>
        /// <param name="item"> <see cref="T"/></param>
        /// <returns></returns>
        Task Add(T item);

        /// <summary>
        /// Delete <see cref="T"/> from DbSet
        /// </summary>
        /// <param name="id"> Id of <see cref="T"/></param>
        /// <returns></returns>
        Task Delete(int id);

        /// <summary>
        /// Update <see cref="T"/>
        /// </summary>
        /// <param name="item"><see cref="T"/></param>
        /// <returns></returns>
        Task Update(T item);
    }
}
