﻿namespace BLL.Infrastructure
{
    /// <summary>
    /// Add operation detail for Identity
    /// reference METANIT
    /// </summary>
    public class OperationDetails
    {
        /// <summary>
        /// Constructor for <see cref="OperationDetails"/>
        /// </summary>
        /// <param name="succedeed"></param>
        /// <param name="message"></param>
        /// <param name="prop"></param>
        public OperationDetails(bool succedeed, string message, string prop)
        {
            Succedeed = succedeed;
            Message = message;
            Property = prop;
        }

        /// <summary>
        /// Is operation success
        /// </summary>
        public bool Succedeed { get; private set; }

        /// <summary>
        /// Operation message , may using validation
        /// </summary>
        public string Message { get; private set; }
        /// <summary>
        /// For what property was action done
        /// </summary>
        public string Property { get; private set; }
    }
}
