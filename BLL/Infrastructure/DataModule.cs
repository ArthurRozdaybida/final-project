﻿using DAL.Interfaces;
using DAL.Repositories;
using Ninject.Modules;

namespace BLL.Infrastructure
{
    /// <summary>
    /// Create DI for UoW and IUoW
    /// </summary>
    public class DataModule : NinjectModule
    {
        /// <summary>
        /// Connectionstring for DataContext
        /// </summary>
        private string connectionString;

        /// <summary>
        /// Constructor for <see cref="DataModule"/>
        /// </summary>
        /// <param name="connection"></param>
        public DataModule(string connection)
        {
            connectionString = connection;
        }

        /// <summary>
        /// Create DI
        /// </summary>
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument(connectionString);
        }
    }

}
